package org.nrg.xapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.model.users.User;
import org.nrg.xapi.services.CARManagementService;
import org.nrg.xapi.services.CohortAccessRequestService;
import org.nrg.xapi.services.SubcohortService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class TestCohortAccessRequest {

    @Test
    public void testCohortAccessRequestFromScratch() throws JsonProcessingException {
        final CohortAccessRequest request = new CohortAccessRequest();
        request.setUsername(_user.getUsername());
        request.setFullname(_user.getFirstName() + " " + _user.getLastName());
        request.setTitle("that thing");
        request.setDescription("The one I like");
        request.setStatus(CohortStatus.Pending);
        _requestService.create(request);
        _subcohortService.create(request, "foo", 50, "Ah man", Arrays.asList("foo@bar.com", "example@place.com"));
        _subcohortService.create(request, "bar", 50, "Ah man", Arrays.asList("bar@foo.com", "place@example.com"));
        _requestService.refresh(request);
        final List<CohortAccessRequest> cars = _service.getCarsForUser(_user);
        assertNotNull(cars);
        assertEquals(1, cars.size());
        assertEquals(_user.getUsername(), cars.get(0).getUsername());
        assertNotNull(cars.get(0).getCohorts());
        assertEquals(2, cars.get(0).getCohorts().size());
        System.out.println(_mapper.writeValueAsString(request));
    }

    @Test
    public void testCohortAccessRequestFromScratchWithoutDescription() throws JsonProcessingException {
        final CohortAccessRequest request = new CohortAccessRequest(_user.getUsername(), _user.getFirstName() + " " + _user.getLastName(), "that thing");
        _requestService.create(request);
        _subcohortService.create(request, "foo", 50, "Ah man", Arrays.asList("foo@bar.com", "example@place.com"));
        _subcohortService.create(request, "bar", 50, "Ah man", Arrays.asList("bar@foo.com", "place@example.com"));
        _requestService.refresh(request);
        final List<CohortAccessRequest> cars = _service.getCarsForUser(_user);
        assertNotNull(cars);
        assertEquals(1, cars.size());
        assertEquals(_user.getUsername(), cars.get(0).getUsername());
        assertNotNull(cars.get(0).getCohorts());
        assertEquals(2, cars.get(0).getCohorts().size());
        System.out.println(_mapper.writeValueAsString(request));
    }

    @Test
    public void testCohortAccessRequest() throws JsonProcessingException {
        final CohortAccessRequest request = _requestService.create("me", "that thing", "The one I like");
        _subcohortService.create(request, "foo", 50, "Ah man", Arrays.asList("foo@bar.com", "example@place.com"));
        _subcohortService.create(request, "bar", 50, "Ah man", Arrays.asList("bar@foo.com", "place@example.com"));
        _requestService.refresh(request);
        final List<CohortAccessRequest> cars = _service.getCarsForUser(_user);
        assertNotNull(cars);
        assertEquals(1, cars.size());
        assertEquals(_user.getUsername(), cars.get(0).getUsername());
        assertNotNull(cars.get(0).getCohorts());
        assertEquals(2, cars.get(0).getCohorts().size());
        System.out.println(_mapper.writeValueAsString(request));
    }

    @Test
    public void testCohortAccessRequestCreate() throws IOException {
        final CohortAccessRequest request = new CohortAccessRequest("me", "that thing", "The one I love");
        final Subcohort subcohort1 = new Subcohort("foo", 50, "Ah man", Arrays.asList("foo@bar.com", "example@place.com"));
        final Subcohort subcohort2 = new Subcohort("bar", 50, "Ah man", Arrays.asList("bar@foo.com", "place@example.com"));
        _service.create(request, subcohort1, subcohort2);

        final List<CohortAccessRequest> cars = _service.getPendingCars();

        assertNotNull(cars);
        assertEquals(1, cars.size());
        assertEquals(_user.getUsername(), cars.get(0).getUsername());
        assertNotNull(cars.get(0).getCohorts());
        assertEquals(2, cars.get(0).getCohorts().size());
        final String json = _mapper.writeValueAsString(request);
        final CohortAccessRequest deserialized = _mapper.readValue(json, _carJavaType);
        assertNotNull(deserialized);
    }

    private static final ObjectMapper _mapper = new ObjectMapper();
    private static final JavaType _carJavaType = _mapper.getTypeFactory().constructType(CohortAccessRequest.class);
    private static final User _user = new User();
    static {
        _user.setUsername("me");
    }

    @Inject
    private CARManagementService _service;

    @Inject
    private CohortAccessRequestService _requestService;

    @Inject
    private SubcohortService _subcohortService;
}
