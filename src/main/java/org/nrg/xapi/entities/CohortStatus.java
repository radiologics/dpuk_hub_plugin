package org.nrg.xapi.entities;

public enum CohortStatus {
    Pending,
    Rejected,
    Approved,
    Cancelled,
    In_Progress,
    Complete
}
