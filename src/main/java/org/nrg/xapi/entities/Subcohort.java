package org.nrg.xapi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;

import javax.persistence.*;
import java.util.*;

@Auditable
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"request", "project", "disabled"}))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class Subcohort extends AbstractHibernateEntity {
	private static final long serialVersionUID = -8786180524031841643L;
	public Subcohort() {
        _status = CohortStatus.Pending;
    }

    public Subcohort(final String project, final Integer subjectCount, final String study, final List<String> owners) {
        this();
        setProject(project);
        setSubjectCount(subjectCount);
        setStudy(study);
        setOwners(owners);
    }

    public Subcohort(final CohortAccessRequest request, final String project, final Integer subjectCount, final String study, final List<String> owners) {
        this();
        setRequest(request);
        setProject(project);
        setSubjectCount(subjectCount);
        setStudy(study);
        setOwners(owners);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    public CohortAccessRequest getRequest() {
        return _request;
    }

    public void setRequest(final CohortAccessRequest request) {
        _request = request;
    }

    public String getProject() {
        return _project;
    }

    public void setProject(final String project) {
        _project = project;
    }

    public String getProjectDisplay() {
        return _projectDisplay;
    }

    public void setProjectDisplay(final String project) {
        _projectDisplay = project;
    }

    public int getSubjectCount() {
        return _subjectCount;
    }

    public void setSubjectCount(final int subjectCount) {
        _subjectCount = subjectCount;
    }

    public String getStudy() {
        return _study;
    }

    public void setStudy(final String study) {
        _study = study;
    }

    public String getStudyDisplay() {
        return _studyDisplay;
    }

    public void setStudyDisplay(final String study) {
    	_studyDisplay = study;
    }

    @ElementCollection(targetClass = java.lang.String.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<String> getOwners() {
        return _owners;
    }

    public void setOwners(final List<String> owners) {
        _owners = owners;
    }

    public CohortStatus getStatus() {
        return _status;
    }

    public void setStatus(final CohortStatus status) {
        _status = status;
    }

    public boolean updateFrom(final Subcohort subcohort) {
        boolean isDirty = false;
        final String project = subcohort.getProject();
        if (StringUtils.isNotBlank(project) && !StringUtils.equals(project, _project)) {
            isDirty = true;
            setProject(project);
        }
        final int subjectCount = subcohort.getSubjectCount();
        if (subjectCount != 0 && subjectCount != _subjectCount) {
            isDirty = true;
            setSubjectCount(subjectCount);
        }
        final String study = subcohort.getStudy();
        if (StringUtils.isNotBlank(study) && !StringUtils.equals(study, _study)) {
            isDirty = true;
            setStudy(study);
        }
        final List<String> owners = subcohort.getOwners();
        if (owners != null && owners.size() > 0 && !owners.equals(_owners)) {
            isDirty = true;
            setOwners(owners);
        }
        final CohortStatus status = subcohort.getStatus();
        if (status != null && status != _status) {
            isDirty = true;
            setStatus(status);
        }
        return isDirty;
    }

    @Override
    public String toString() {
        return _project + " (" + getSubjectCount() + " subjects)";
    }

    private CohortAccessRequest _request;
    private String _project;
    private String _projectDisplay;
    private int _subjectCount;
    private String _study;
    private String _studyDisplay;
    private List<String> _owners;
    private CohortStatus _status;
}
