package org.nrg.xapi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;
import javax.persistence.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Auditable
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"title", "username", "disabled"}))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class CohortAccessRequest extends AbstractHibernateEntity {
	private static final long serialVersionUID = 4896304712179860487L;
	private String _title;
    private String _createdProject;
    private String _username;
    private String _fullname;
    private String _description;
    private String _search;
    private String _results;
    private CohortStatus _status;
    private List<Subcohort> _cohorts;
    private final Map<Long, Subcohort> _cohortsAsMap = new HashMap<>();

    public CohortAccessRequest() {
    }

    public CohortAccessRequest(final String username, final String fullname, final String title) {
        this(username, fullname, title, null);
    }

    public CohortAccessRequest(final String username, final String fullname, final String title, final String description) {
        setUsername(username);
        setFullname(fullname);
        setTitle(title);
        setDescription(description);
        setStatus(CohortStatus.Pending);
    }

    @Column(nullable = false)
    public String getTitle() {
        return _title;
    }

    public void setTitle(final String title) {
        _title = title;
    }

    @Column(nullable = true)
    public String getCreatedProject() {
        return _createdProject;
    }

    public void setCreatedProject(final String newProject) {
    	_createdProject = newProject;
    }

    @Column(nullable = false)
    public String getUsername() {
        return _username;
    }

    public void setUsername(final String username) {
        _username = username;
    }

    @Column(nullable = false)
    public String getFullname() {
        return _fullname;
    }

    public void setFullname(final String fullname) {
        _fullname = fullname;
    }

    @Column(columnDefinition = "TEXT")
    public String getDescription() {
        return _description;
    }

    public void setDescription(final String description) {
        _description = description;
    }

    @Column(columnDefinition = "TEXT")
    @JsonIgnore
    public String getResults() {
        return _results;
    }

    public void setResults(final String results) {
    	_results = results;
    }

    /**
     * Retrieves the matching subject IDs.
	 * @return List<List> : Inner list has 3 columns study,project,subject
     * @return
     */
    @Transient
    @JsonIgnore
    public List<String[]> getResultsAsList() {
    	List<String[]> subjects=Lists.newArrayList();
        for(String line: StringUtils.split(_results, "\n")){
        	subjects.add(StringUtils.split(line,","));
        }
        return subjects;
    }

    public void setResults(final List<Object[]> subjects) {
    	//there's got to be a better way to do this.
		//but I don't want a separate table for the subject rows.
		//so, I just flatten it to a comma delimited string (which seems like the most efficient size-wise)
		StringBuilder sb=new StringBuilder();
		for(Object[] subject: subjects){
			sb.append(subject[0]).append(",").append(subject[1]).append(",").append(subject[2]).append("\n");
		}
		_results = sb.toString();
    }

    @JsonIgnore
    @Column(columnDefinition = "TEXT")
    public String getSearch() {
        return _search;
    }

    public void setSearch(final String search) {
        _search = search;
    }

    @Column(nullable = false)
    public CohortStatus getStatus() {
        return _status;
    }

    public void setStatus(final CohortStatus status) {
        _status = status;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "request")
    @JsonManagedReference
    public List<Subcohort> getCohorts() {
        return _cohorts;
    }

    @SuppressWarnings("unused")
    public void setCohorts(final List<Subcohort> cohorts) {
        _cohorts = cohorts;
    }

    @Transient
    @JsonIgnore
    public Subcohort getCohort(final long cohortId) {
        return getCohortsAsMap().get(cohortId);
    }

    @Transient
    @JsonIgnore
    public Map<Long, Subcohort> getCohortsAsMap() {
        if (_cohorts == null) {
            return new HashMap<>();
        }
        if (_cohortsAsMap.size() == 0 && _cohorts.size() > 0) {
            for (final Subcohort cohort : _cohorts) {
                _cohortsAsMap.put(cohort.getId(), cohort);
            }
        }
        return new HashMap<>(_cohortsAsMap);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final CohortAccessRequest request = (CohortAccessRequest) o;

        return getTitle().equals(request.getTitle()) &&
                getUsername().equals(request.getUsername()) &&
                getDescription().equals(request.getDescription()) &&
                (getSearch() != null ? getSearch().equals(request.getSearch()) : request.getSearch() == null &&
                		getResults() != null ? getResults().equals(request.getResults()) : request.getResults() == null &&	
                		getCreatedProject() != null ? getCreatedProject().equals(request.getCreatedProject()) : request.getCreatedProject() == null &&	
                		getStatus() == request.getStatus() &&
                	(getCohorts() != null ? getCohorts().equals(request.getCohorts()) : request.getCohorts() == null)
                );
    }

    @Override
    public int hashCode() {
        int result = getTitle().hashCode();
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + (getCreatedProject() != null ? getCreatedProject().hashCode() : 0);
        result = 31 * result + (getSearch() != null ? getSearch().hashCode() : 0);
        result = 31 * result + (getResults() != null ? getResults().hashCode() : 0);
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + (getCohorts() != null ? getCohorts().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder(getClass().getSimpleName());
        buffer.append(": ").append(getTitle()).append(" ").append(getUsername()).append(" (").append(getStatus()).append(")");
        if (getCohorts() != null && getCohorts().size() > 0) {
            buffer.append(" ").append(getCohorts().size()).append(" cohorts");
        }
        return buffer.toString();
    }

    public static class CarComparator implements Comparator<CohortAccessRequest> {
        @Override
        public int compare(final CohortAccessRequest request1, final CohortAccessRequest request2) {
            final boolean isRequest1Null = request1 == null;
            final boolean isRequest2Null = request2 == null;
            if (isRequest1Null && isRequest2Null) {
                return 0;
            }
            if (isRequest1Null) {
                return -1;
            }
            if (isRequest2Null) {
                return 1;
            }
            return request1.getCreated().compareTo(request2.getCreated());
        }
    }
    
}
