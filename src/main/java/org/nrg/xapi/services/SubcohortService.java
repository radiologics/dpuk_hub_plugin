package org.nrg.xapi.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;

import java.util.List;

public interface SubcohortService extends BaseHibernateService<Subcohort> {
    List<Subcohort> getCohortsByProject(final String projectId);

    List<Subcohort> getCohortsByProjectAndStatus(final String projectId, final CohortStatus status);
}
