package org.nrg.xapi.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xapi.model.users.User;
import org.nrg.xapi.entities.CohortAccessRequest;

import java.util.List;

public interface CohortAccessRequestService extends BaseHibernateService<CohortAccessRequest> {
    List<CohortAccessRequest> getCarsForUser(final User user);
    List<CohortAccessRequest> getPendingCars();
}
