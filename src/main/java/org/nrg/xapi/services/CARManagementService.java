package org.nrg.xapi.services;

import org.hibernate.ObjectNotFoundException;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.model.users.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CARManagementService {
    // CohortAccessRequest management methods
    public CohortAccessRequest create(final CohortAccessRequest request, final Subcohort... subcohorts) {
        _requestService.create(request);
        if (request.getCohorts() != null) {
            for (final Subcohort subcohort : request.getCohorts()) {
                subcohort.setRequest(request);
                createOrUpdate(subcohort);
            }
        }
        for (final Subcohort subcohort : subcohorts) {
            subcohort.setRequest(request);
            _subcohortService.create(subcohort);
        }
        _requestService.refresh(request);
        return request;
    }

    public CohortAccessRequest retrieve(final long id) {
        return _requestService.retrieve(id);
    }

    public void update(final CohortAccessRequest request, final Subcohort... subcohorts) {
        _requestService.update(request);
        for (final Subcohort subcohort : subcohorts) {
            subcohort.setRequest(request);
            createOrUpdate(subcohort);
        }
        _requestService.refresh(request);
    }

    public void delete(final CohortAccessRequest request) {
        _requestService.delete(request);
    }

    public void refresh(final CohortAccessRequest request) {
        _requestService.refresh(request);
    }

    // Subcohort management methods
    public Subcohort createOrUpdate(final Subcohort subcohort) {
        try {
            final Subcohort existing = _subcohortService.retrieve(subcohort.getId());
            existing.updateFrom(subcohort);
            _subcohortService.update(existing);
        } catch (final ObjectNotFoundException ignored) {
            _subcohortService.create(subcohort);
        }
        return subcohort;
    }

    public void update(final Subcohort subcohort) {
        _subcohortService.update(subcohort);
    }

    public void delete(final Subcohort subcohort) {
        _subcohortService.delete(subcohort);
    }

    public List<CohortAccessRequest> getCarsForUser(final User user) {
        return _requestService.getCarsForUser(user);
    }

    public List<CohortAccessRequest> getPendingCars() {
        return _requestService.getPendingCars();
    }

    public List<CohortAccessRequest> getCohortAccessRequestsForProject(final String projectId) {
        final List<Subcohort> subcohorts = _subcohortService.getCohortsByProject(projectId);
        if (subcohorts == null || subcohorts.size() == 0) {
            return new ArrayList<>();
        }
        final Set<CohortAccessRequest> requests = new HashSet<>();
        for (final Subcohort subcohort : subcohorts) {
            requests.add(subcohort.getRequest());
        }
        return new ArrayList<>(requests);
    }

    public List<Subcohort> getCohortsForProject(final String projectId) {
        final List<Subcohort> subcohorts = _subcohortService.getCohortsByProject(projectId);
        if (subcohorts == null || subcohorts.size() == 0) {
            return new ArrayList<>();
        }
        return subcohorts;
    }

    public List<Subcohort> getCohortsForProjectAndStatus(final String projectId, final CohortStatus status) {
        final List<Subcohort> subcohorts = _subcohortService.getCohortsByProjectAndStatus(projectId, status);
        if (subcohorts == null || subcohorts.size() == 0) {
            return new ArrayList<>();
        }
        return subcohorts;
    }

    @Inject
    private CohortAccessRequestService _requestService;

    @Inject
    private SubcohortService _subcohortService;
}
