package org.nrg.xapi.services.impl.hibernate;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.model.users.User;
import org.nrg.xapi.repositories.CohortAccessRequestDAO;
import org.nrg.xapi.services.CohortAccessRequestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HibernateCohortAccessRequestService extends AbstractHibernateEntityService<CohortAccessRequest, CohortAccessRequestDAO> implements CohortAccessRequestService {
    @Override
    @Transactional
    public List<CohortAccessRequest> getCarsForUser(final User user) {
        final Map<String, Object> properties = new HashMap<>();
        properties.put("username", user.getUsername());
        return getDao().findByProperties(properties);
    }

    @Override
    @Transactional
    public List<CohortAccessRequest> getPendingCars() {
        return getDao().findByProperties(_openCarsProperties);
    }

    private static final Map<String, Object> _openCarsProperties = new HashMap<String, Object>() {
                     private static final long serialVersionUID = -1879623281836667188L;
                     {
                         put("status", CohortStatus.Pending);
                     }
    };
}
