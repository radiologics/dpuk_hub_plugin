package org.nrg.xapi.services.impl.hibernate;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.repositories.SubcohortDAO;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.services.SubcohortService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HibernateSubcohortService extends AbstractHibernateEntityService<Subcohort, SubcohortDAO> implements SubcohortService {
    @Override
    @Transactional
    public List<Subcohort> getCohortsByProject(final String projectId) {
        return getCohortsByProjectAndStatus(projectId, null);
    }

    @Override
    @Transactional
    public List<Subcohort> getCohortsByProjectAndStatus(final String projectId, final CohortStatus status) {
        final Map<String, Object> properties = new HashMap<>();
        properties.put("project", projectId);
        if (status != null) {
            properties.put("status", status);
        }
        return getDao().findByProperties(properties);
    }
}
