package org.nrg.xapi.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiOperation;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.security.UserI;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.services.CohortAccessRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Api("API for administrative operations for cohort access requests (CARs)")
@XapiRestController
@RequestMapping(value = {"/admin/access/cars"}, produces = {"application/json"})
public class AdminCohortAccessRequestApi {
    private static final Logger _log = LoggerFactory.getLogger(AdminCohortAccessRequestApi.class);

    @ApiOperation(value = "Get list of all CARs in the system.", notes = "This function returns a list of all CARs in the system. This is only accessible to system administrators.", response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "A list of cohort access requests on the system"),
                  @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                  @ApiResponse(code = 403, message = "Not authorized to access this function."),
                  @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/admin"}, produces = {"application/json", "application/xml"}, method = {RequestMethod.GET})
    public ResponseEntity<List<CohortAccessRequest>> carsGetAll() throws NotFoundException {
        final HttpStatus status = isPermitted();
        if (status != null) {
            if (_log.isWarnEnabled()) {
                final UserI user = getSessionUser();
                _log.warn("Non-admin user {} tried to access this admin-only function.", user != null ? user.getUsername() : "Unknown");
            }
            return new ResponseEntity<>(status);
        }

        if (_log.isInfoEnabled()) {
            final UserI user = getSessionUser();
            assert user != null;
            _log.info("Admin user {} accessed this admin-only function.", user.getUsername());
        }

        final ArrayList<CohortAccessRequest> cars = new ArrayList<>(_service.getAll());
        Collections.sort(cars, new CohortAccessRequest.CarComparator());
        return new ResponseEntity<List<CohortAccessRequest>>(cars, HttpStatus.OK);
    }

    private UserI getSessionUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if ((principal instanceof UserI)) {
            return (UserI) principal;
        }
        return null;
    }

    private HttpStatus isPermitted() {
        UserI sessionUser = getSessionUser();
        if ((sessionUser instanceof XDATUser)) {
            return ((XDATUser) sessionUser).isSiteAdmin() ? null : HttpStatus.FORBIDDEN;
        }

        return null;
    }

    @Inject
    private CohortAccessRequestService _service;
}
