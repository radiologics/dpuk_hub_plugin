package org.nrg.xapi.api;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.model.users.User;
import org.nrg.xapi.model.users.UserFactory;
import org.nrg.xapi.services.CARManagementService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Joiner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;



@Api("API for managing cohort access requests (CARs)")
@XapiRestController
@RequestMapping("/access/cars")
public class CohortAccessRequestApi {
    private static final Logger _log = LoggerFactory.getLogger(CohortAccessRequestApi.class);

    @ApiOperation(value = "Get list of all CARs for the current user.", notes = "This function returns a list of all CARs for the current user.",  response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "A list of open cohort access requests for the current user"),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {""}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<List<CohortAccessRequest>> carsGet() throws NotFoundException {
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        final User user = userFactory.getUser(getSessionUser());
        final List<CohortAccessRequest> cars = _service.getCarsForUser(user);
        if (cars == null || cars.size() == 0) {
            if (_log.isDebugEnabled()) {
                _log.debug("Found no CARs for the user {}.", user.getUsername());
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }
        if (_log.isDebugEnabled()) {
            _log.debug("Found {} CARs for the user {}.", cars.size(), user.getUsername());
        }
        for (final CohortAccessRequest car : cars) {
            for (final Subcohort subcohort : car.getCohorts()) {
                _log.debug("Subcohort: " + subcohort.toString());
            }
        }
        Collections.sort(cars, new CohortAccessRequest.CarComparator());
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @ApiOperation(value = "Creates a new CAR.", notes = "Creates a new CAR.",  response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The new CAR was successfully created."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {""}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.POST})
    public ResponseEntity<CohortAccessRequest> carsPost(@ApiParam(value = "The serialized CAR to be created.", required = true) @RequestBody final CohortAccessRequest car) throws NotFoundException {
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        final User user = userFactory.getUser(getSessionUser());
        car.setUsername(user.getUsername());
        car.setFullname(user.getFirstName() + " " + user.getLastName());
        _service.create(car);
        
        sendCARCreateEmail(car);
        if (_log.isInfoEnabled()) {
            final List<Subcohort> cohorts = car.getCohorts();
            _log.info("Created a new CAR with ID for user {}, requesting access to {} cohorts: {}", car.getId(), user.getUsername(), car.getCohorts().size(), Joiner.on(", ").join(cohorts));
        }
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @ApiOperation(value = "Get the specified CAR.", notes = "Returns the CAR with the specified ID.", response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "CAR successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access the XNAT REST API."),
                   @ApiResponse(code = 404, message = "CAR not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<CohortAccessRequest> carsIdGet(@ApiParam(value = "The ID of the CAR to fetch", required = true) @PathVariable final long id) throws NotFoundException {
        final CohortAccessRequest car = _service.retrieve(id);
        final HttpStatus status = isPermitted(car);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @ApiOperation(value = "Updates the CAR with the specified ID.", notes = "Returns the updated serialized CAR.", response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "CAR successfully updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to create or update this CAR."),
                   @ApiResponse(code = 404, message = "CAR not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/{id}"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.PUT})
    public ResponseEntity<CohortAccessRequest> carsIdPut(@ApiParam(value = "The ID of the CAR to update.", required = true) @PathVariable final long id,
                                                         @ApiParam(value = "The serialized CAR to be created.", required = true) @RequestBody final CohortAccessRequest car) throws NotFoundException {
        final CohortAccessRequest retrieved = _service.retrieve(id);
        if (retrieved == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final HttpStatus status = isPermitted(retrieved);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        if (retrieved.equals(car)) {
            return new ResponseEntity<>(car, HttpStatus.OK);
        }
        updateFromModel(retrieved, car);
        return new ResponseEntity<>(retrieved, HttpStatus.OK);
    }

    @ApiOperation(value = "Deletes the CAR with the specified ID.")
    @ApiResponses({@ApiResponse(code = 200, message = "CAR successfully deleted."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to delete this CAR."),
                   @ApiResponse(code = 404, message = "CAR not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/{id}"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.DELETE})
    public ResponseEntity<Void> carsIdDelete(@ApiParam(value = "The ID of the CAR to update.", required = true) @PathVariable final long id) throws NotFoundException {
        final CohortAccessRequest retrieved = _service.retrieve(id);
        if (retrieved == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final HttpStatus status = isPermitted(retrieved);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        _service.delete(retrieved);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "Get a particular subcohort for the specified CAR.", notes = "Returns the subcohort for the CAR with the specified ID.", response = org.nrg.xapi.entities.Subcohort.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Subcohort successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access the XNAT REST API."),
                   @ApiResponse(code = 404, message = "CAR not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/{id}/{cohortId}"}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<Subcohort> carsCohortIdGet(@ApiParam(value = "The ID of the CAR containing the desired subcohort", required = true) @PathVariable final long id,
                                                     @ApiParam(value = "The project of the subcohort to fetch", required = true) @PathVariable final long cohortId) throws NotFoundException {
        final CohortAccessRequest car = _service.retrieve(id);
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        if (car == null) {
            final User user = userFactory.getUser(getSessionUser());
            if (_log.isInfoEnabled()) {
                _log.info("User {} requested the CAR {}, but that doesn't exist.", user.getUsername(), id);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final HttpStatus status = isPermitted(car);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        final Subcohort subcohort = car.getCohort(cohortId);
        if (subcohort == null) {
            final User user = userFactory.getUser(getSessionUser());
            if (_log.isInfoEnabled()) {
                _log.info("User {} requested the subcohort {} from the CAR {}, but that doesn't exist.", user.getUsername(), cohortId, id);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (_log.isDebugEnabled()) {
            final User user = userFactory.getUser(getSessionUser());
            _log.debug("User {} requested the subcohort {} from the CAR {}. Subcohort was located and contains {} subjects.", user.getUsername(), cohortId, id, subcohort.getSubjectCount());
        }
        return new ResponseEntity<>(subcohort, HttpStatus.OK);
    }
    
    private static boolean isCohortApproved(final CohortAccessRequest car){
    	if(!CohortStatus.Approved.equals(car.getStatus())){
    		return false;
    	}
    	
    	for(Subcohort cohort: car.getCohorts()){
			if(!(CohortStatus.Approved.equals(cohort.getStatus()) || CohortStatus.Rejected.equals(cohort.getStatus()))){
				return false;
			}
		}
    	
    	return true;
    }
    
    private void sendApprovalEmail(final CohortAccessRequest car){
		try {
			String huburl=XDAT.getSiteConfigurationProperty("Access-Control-Allow-Origin");
			
			StringBuilder sb =new StringBuilder();
			sb.append("The access request you submitted on ").append(car.getCreated()).append(" titled '").append(car.getTitle()).append("' has been reviewed by DPUK staff and your access has been approved.  <br/><br/>Please proceed to the DPUK Hub website (Manage My Access Requests) to import the data for your research.<br/><br/><a href='").append(huburl).append("/#/my-requests'>").append(huburl).append("</a><br/><br/>The DPUK Team");
			
			XDATUser _user=new XDATUser(car.getUsername());
	    	String from = XDAT.getSiteConfigPreferences().getAdminEmail();
			final String[] tos = {_user.getEmail()};
			
			XDAT.getMailService().sendHtmlMessage(from, tos, TurbineUtils.GetSystemName()+": Access request approved", sb.toString());
		} catch (Throwable e) {
			_log.error("",e);
		}
    }
    
    private void sendCARCreateEmail(final CohortAccessRequest car){
		try {
			String huburl=XDAT.getSiteConfigurationProperty("Access-Control-Allow-Origin");
			XDATUser _user=new XDATUser(car.getUsername());
			
			StringBuilder sb =new StringBuilder();
			sb.append("User " + _user.getLogin() + " has created a new Access Request, titled '").append(car.getTitle()).append("'.<br/><br/><a href='").append(huburl).append("/#/admin-access-requests'>").append(huburl).append("</a><br/><br/>The DPUK Team");
			
	    	String from = XDAT.getSiteConfigPreferences().getAdminEmail();
			final String[] tos = {from};
			
			XDAT.getMailService().sendHtmlMessage(from, tos, TurbineUtils.GetSystemName()+": Access request created", sb.toString());
		} catch (Throwable e) {
			_log.error("",e);
		}
    }
    
    private void sendCARUpdateEmail(final CohortAccessRequest car){
		try {
			String huburl=XDAT.getSiteConfigurationProperty("Access-Control-Allow-Origin");
			XDATUser _user=new XDATUser(car.getUsername());
			
			StringBuilder sb =new StringBuilder();
			sb.append("User " + _user.getLogin() + " has updated the '" + car.getStatus() + "' Access Request, titled '").append(car.getTitle()).append("'.<br/><br/><a href='").append(huburl).append("/#/admin-access-requests'>").append(huburl).append("</a><br/><br/>The DPUK Team");
			
	    	String from = XDAT.getSiteConfigPreferences().getAdminEmail();
			final String[] tos = {from};
			
			XDAT.getMailService().sendHtmlMessage(from, tos, TurbineUtils.GetSystemName()+": Access request updated", sb.toString());
		} catch (Throwable e) {
			_log.error("",e);
		}
    }

    @ApiOperation(value = "Add or updates the submitted subcohort in the CAR with the specified ID.", notes = "Returns the updated serialized CAR.", response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "CAR successfully created or updated."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to create or update this CAR."),
                   @ApiResponse(code = 404, message = "CAR not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/{id}/{cohortId}"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.PUT})
    public ResponseEntity<CohortAccessRequest> carsCohortIdPut(@ApiParam(value = "The ID of the user to create or update.", required = true) @PathVariable final long id,
                                                               @ApiParam(value = "The ", required = true) @PathVariable final long cohortId,
                                                               @ApiParam(value = "", required = true) @RequestBody final Subcohort subcohort) throws NotFoundException {
        final CohortAccessRequest car = _service.retrieve(id);
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        final boolean wasApproved=isCohortApproved(car);
        
        if (car == null) {
            if (_log.isInfoEnabled()) {
                final User user = userFactory.getUser(getSessionUser());
                _log.info("User {} tried to update the CAR {}, but that doesn't exist.", user.getUsername(), id);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final HttpStatus status = isPermitted(car);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        final Subcohort existing = car.getCohort(cohortId);
        if (existing == null) {
            if (_log.isInfoEnabled()) {
                final User user = userFactory.getUser(getSessionUser());
                _log.info("User {} is putting the subcohort {} into the CAR {}, this is a new subcohort.", user.getUsername(), cohortId, id);
            }
            _service.createOrUpdate(subcohort); // TODO: Is this necessary? Does the subcohort need to be created separately or will the update of the CAR save the subcohort automatically?
            return new ResponseEntity<>(car, HttpStatus.OK);
        } else {
            final boolean isDirty = existing.updateFrom(subcohort);
            if (isDirty) {
                _service.update(existing); // TODO: Is this necessary? Does the subcohort need to be updated separately or will the update of the CAR save the subcohort automatically?
            }
        }
        _service.refresh(car);
        
        if(isCohortApproved(car) && !wasApproved){
        	//this car is now approved for the first time.
        	sendApprovalEmail(car);
        }
        
        return new ResponseEntity<>(car, HttpStatus.OK);
    }

    @ApiOperation(value = "Get list of all CARs containing requests for access to the specified project.", notes = "This function returns a list of all CARs that contain a subcohort from the indicated project.",  response = org.nrg.xapi.entities.CohortAccessRequest.class)
    @ApiResponses({@ApiResponse(code = 200, message = "A list of cohort access requests that include the specified project"),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access this function."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/project/{projectId}"}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<List<CohortAccessRequest>> carsProjectIdGet(@ApiParam(value = "The ID of the project on which you're querying", required = true) @PathVariable final String projectId) throws NotFoundException {
        final HttpStatus status = isPermitted();
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        final List<CohortAccessRequest> requests = _service.getCohortAccessRequestsForProject(projectId);
        if (requests == null || requests.size() == 0) {
            if (_log.isDebugEnabled()) {
                _log.debug("Found no CARs for the specified project {}.", projectId);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }
        if (_log.isDebugEnabled()) {
            _log.debug("Found {} CARs for the project {}.", requests.size(), projectId);
        }
        for (final CohortAccessRequest request : requests) {
            for (final Subcohort subcohort : request.getCohorts()) {
                _log.debug("Subcohort: " + subcohort.toString());
            }
        }
        Collections.sort(requests, new CohortAccessRequest.CarComparator());
        return new ResponseEntity<>(requests, HttpStatus.OK);
    }

    @ApiOperation(value = "Gets all cohorts associated with a particular project.", notes = "Returns a list of cohorts for the indicated project from all requests.",  response = org.nrg.xapi.entities.Subcohort.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Cohorts successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access the XNAT REST API."),
                   @ApiResponse(code = 404, message = "Project not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/cohorts/{projectId}"}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<List<Subcohort>> carsCohortByProjectGet(@ApiParam(value = "The ID of the project for which you want to related cohorts.", required = true) @PathVariable final String projectId) throws NotFoundException {
        final HttpStatus status = isPermitted();
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        if (status != null) {
            return new ResponseEntity<>(status);
        }
        final List<Subcohort> cohorts = _service.getCohortsForProject(projectId);
        if (cohorts == null) {

            final User user = userFactory.getUser(getSessionUser());
            if (_log.isInfoEnabled()) {
                _log.info("User {} requested all cohorts associated with the project {}, but couldn't find any.", user.getUsername(), projectId);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (_log.isDebugEnabled()) {
            final User user = userFactory.getUser(getSessionUser());
            _log.debug("User {} requested all cohorts associated with the project {}. {} cohorts were located.", user.getUsername(), projectId, cohorts.size());
        }
        return new ResponseEntity<>(cohorts, HttpStatus.OK);
    }

    @ApiOperation(value = "Gets all cohorts associated with a particular project where the cohort has the indicated status.", notes = "Returns a list of cohorts with the indicated status for the indicated project from all requests.", response = org.nrg.xapi.entities.Subcohort.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Cohorts successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to access the XNAT REST API."),
                   @ApiResponse(code = 404, message = "Project not found."),
                   @ApiResponse(code = 500, message = "Unexpected error")})
    @RequestMapping(value = {"/cohorts/{projectId}/{status}"}, produces = MediaType.APPLICATION_JSON_VALUE, method = {RequestMethod.GET})
    public ResponseEntity<List<Subcohort>> carsCohortByProjectAndStatusGet(@ApiParam(value = "The ID of the project for which you want to related cohorts.", required = true) @PathVariable final String projectId, @ApiParam(value = "The status of the cohorts you want to retrieve.", required = true) @PathVariable final CohortStatus status) throws NotFoundException {
        final HttpStatus httpStatus = isPermitted();
        final UserFactory userFactory = XDAT.getContextService().getBean(UserFactory.class);
        if (httpStatus != null) {
            return new ResponseEntity<>(httpStatus);
        }
        final List<Subcohort> cohorts = _service.getCohortsForProjectAndStatus(projectId, status);
        if (cohorts == null) {
            final User user = userFactory.getUser(getSessionUser());
            if (_log.isInfoEnabled()) {
                _log.info("User {} requested all cohorts associated with the project {} and the status {}, but couldn't find any.", user.getUsername(), projectId, status);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (_log.isDebugEnabled()) {
            final User user = userFactory.getUser(getSessionUser());
            _log.debug("User {} requested all cohorts associated with the project {} and the status {}. {} cohorts were located.", user.getUsername(), projectId, status, cohorts.size());
        }
        return new ResponseEntity<>(cohorts, HttpStatus.OK);
    }

    private UserI getSessionUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if ((principal instanceof UserI)) {
            return (UserI) principal;
        }
        return null;
    }

    private HttpStatus isPermitted(final CohortAccessRequest request) {
        UserI sessionUser = getSessionUser();
        if (sessionUser == null) {
            return HttpStatus.UNAUTHORIZED;
        }
        if (request == null) {
            return HttpStatus.NOT_FOUND;
        }
        if ((sessionUser.getUsername().equals(request.getUsername())) || (isPermitted() == null)) {
            return null;
        }
        return HttpStatus.FORBIDDEN;
    }

    private HttpStatus isPermitted() {
        UserI sessionUser = getSessionUser();
        if ((sessionUser instanceof XDATUser)) {
            return ((XDATUser) sessionUser).isSiteAdmin() ? null : HttpStatus.FORBIDDEN;
        }

        return null;
    }

    private void updateFromModel(final CohortAccessRequest retrieved, final CohortAccessRequest car) {
        boolean isDirty = false;
        if (StringUtils.isNotBlank(car.getUsername()) && !retrieved.getUsername().equals(car.getUsername())) {
            isDirty = true;
            retrieved.setUsername(car.getUsername());
        }
        if (StringUtils.isNotBlank(car.getFullname()) && !retrieved.getFullname().equals(car.getFullname())) {
            isDirty = true;
            retrieved.setFullname(car.getFullname());
        }
        if (StringUtils.isNotBlank(car.getTitle()) && !retrieved.getTitle().equals(car.getTitle())) {
            isDirty = true;
            retrieved.setTitle(car.getTitle());
        }
        if (StringUtils.isNotBlank(car.getDescription()) && !StringUtils.equals(retrieved.getDescription(), car.getDescription())) {
            isDirty = true;
            retrieved.setDescription(car.getDescription());
        }
        if (car.getStatus() != null && (retrieved.getStatus() == null || !retrieved.getStatus().equals(car.getStatus()))) {
            isDirty = true;
            retrieved.setStatus(car.getStatus());
        }
        if (car.getCohorts() != null && car.getCohorts().size() > 0 && !retrieved.getCohorts().equals(car.getCohorts())) {
            final Collection<Subcohort> added = CollectionUtils.subtract(car.getCohorts(), retrieved.getCohorts());
            final Collection<Subcohort> removed = CollectionUtils.subtract(retrieved.getCohorts(), car.getCohorts());
            for (final Subcohort subcohort : added) {
                isDirty = true;
                subcohort.setRequest(retrieved);
                _service.createOrUpdate(subcohort);
            }
            for (final Subcohort subcohort : removed) {
                isDirty = true;
                _service.delete(subcohort);
            }
        }
        if (isDirty) {
            _service.update(retrieved);
            _service.refresh(retrieved);
            sendCARUpdateEmail(car);
        }
    }

    @Inject
    private CARManagementService _service;
}
