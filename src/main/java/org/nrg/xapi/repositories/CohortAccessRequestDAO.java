package org.nrg.xapi.repositories;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.springframework.stereotype.Repository;

@Repository
public class CohortAccessRequestDAO extends AbstractHibernateDAO<CohortAccessRequest> {
}
