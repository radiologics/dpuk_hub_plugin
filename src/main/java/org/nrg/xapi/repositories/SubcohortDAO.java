package org.nrg.xapi.repositories;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xapi.entities.Subcohort;
import org.springframework.stereotype.Repository;

@Repository
public class SubcohortDAO extends AbstractHibernateDAO<Subcohort> {
}
