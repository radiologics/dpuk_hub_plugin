package org.nrg.xnat.dpukhub.plugin;


import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xnat.restlet.util.SecureResourceParameterMapper;
import org.nrg.xnat.velocity.context.PostAddProjectContextPopulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@XnatPlugin(value = "dpukHubPlugin", name = "1.7 DPUK Hub Plugin", description = "This is the XNAT 1.7 DPUK Hub plugin.",
            entityPackages = {"org.xnat.entities","org.nrg.xnat.services.project.relationship", "org.nrg.xapi.entities"},
            openUrls= {"/data/studysummary","/REST/studysummary","/data/services/studysummary","/REST/services/studysummary"})
@ComponentScan({"org.nrg.xnat.services.publication.impl",
                "org.nrg.xnat.services.investigator",
                "org.nrg.xnat.services.project.study",
                "org.nrg.xnat.services.project.relationship",
                "org.nrg.xnat.services.project.type",
                "org.nrg.xnat.services.dictionary.impl",
                "org.nrg.xnat.services.search",
                "org.nrg.xnat.velocity.context",
                "org.nrg.xnat.restlet.util",
                "org.nrg.xapi.repositories",
                "org.nrg.xapi.services",
                "org.nrg.xapi.api"})
public class DpukHubPlugin {
	
	@Autowired
	PostAddProjectContextPopulator projectTypeContextPopulator;
	
	@Autowired
	SecureResourceParameterMapper dictionaryServiceParameterMapper;
}