package org.nrg.xnat.services.project.study;


public class StudyProperty {
private String key;
private String value;



public StudyProperty() {
	
}
public StudyProperty(String key, String value) {
	this.key=key;
	this.value=value;
}

public String getValue() {
	return value;
}
public void setValue(String value) {
	this.value = value;
}

public String getKey() {
	return key;
}
public void setKey(String key) {
	this.key = key;
}
}
