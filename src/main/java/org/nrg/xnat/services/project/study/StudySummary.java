package org.nrg.xnat.services.project.study;

public class StudySummary {

	Integer studyCount;
	Integer totalSubjectCount;
	Integer totalHealthySubjectCount;
	public Integer getStudyCount() {
		return studyCount;
	}
	public void setStudyCount(Integer studyCount) {
		this.studyCount = studyCount;
	}
	public Integer getTotalSubjectCount() {
		return totalSubjectCount;
	}
	public void setTotalSubjectCount(Integer totalSubjectCount) {
		this.totalSubjectCount = totalSubjectCount;
	}
	public Integer getTotalHealthySubjectCount() {
		return totalHealthySubjectCount;
	}
	public void setTotalHealthySubjectCount(Integer totalHealthySubjectCount) {
		this.totalHealthySubjectCount = totalHealthySubjectCount;
	}
}
