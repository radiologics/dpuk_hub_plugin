package org.nrg.xnat.services.project.relationship;

import java.util.List;

import org.nrg.framework.orm.hibernate.BaseHibernateService;

public interface ProjectRelationshipService extends BaseHibernateService<ProjectRelationship>{
	//public List<ProjectRelationship> getRelationships(String project);
	public List<ProjectRelationship> findByExample(ProjectRelationship projectRelationship);

	public ProjectRelationship addOrUpdateRelationship(ProjectRelationship projectRelationship);
	public ProjectRelationship findById(Long id);
	public List<ProjectRelationship> findAllBy(String proj1, String proj2,String type);

	public void delete(ProjectRelationship projectRelationship);
	public void delete(Long id);
	public void delete(String proj1, String proj2,String type);
}