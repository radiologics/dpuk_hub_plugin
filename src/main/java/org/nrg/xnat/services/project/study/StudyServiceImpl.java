package org.nrg.xnat.services.project.study;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatProjectdataAlias;
import org.nrg.xdat.om.XnatProjectdataField;
import org.nrg.xdat.security.SecurityManager;
import org.nrg.xft.exception.InvalidItemException;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.services.investigator.InvestigatorService;
import org.nrg.xnat.services.publication.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.security.helpers.Permissions;

@Service
@Transactional
public class StudyServiceImpl implements StudyService {
	
	static Logger logger = Logger.getLogger(StudyServiceImpl.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private final String TYPE="study";
	private final String PROTECTED="protected";
	private final String PUBLIC="public";

    @Autowired
    PublicationService publicationService;
    
    
    @Autowired
    InvestigatorService investigatorService;
    
    
	public StudyServiceImpl() {
	}
	
	
	public StudyServiceImpl(PublicationService publicationService,InvestigatorService investigatorService) {
		this.publicationService=publicationService;
		this.investigatorService=investigatorService;
	
	}
	
	
	public List<Study> findEditableByUser(UserI user) throws StudyException{
		List<Study> studies=new ArrayList<Study>();
		try{
			
			List<Object> stuff=Permissions.getAllowedValues(user, "xnat:projectData", "xnat:projectData/ID", SecurityManager.EDIT );
			for (Object id : stuff) {
				String proj=(String)id;
				if(proj!=null){
					Study study=this.findById(user,proj);
					//fix this.
					if(study!=null && StringUtils.equals(TYPE, study.getType())){
						studies.add(study);
					}
				}
			}
		}catch(Exception ex){
			logger.warn("findEditableByUser: no properties found");
		}
		return studies;
	}
	/*
	 * Modified to return only PUBLIC AND PROTECTED projects
	 * (non-Javadoc)
	 * @see org.nrg.xnat.services.project.study.StudyService#findReadableByUser(org.nrg.xdat.security.XDATUser)
	 */
	public List<Study> findReadableByUser(UserI user) throws StudyException{
		List<Study> studies=new ArrayList<Study>();
		try{
			List<Object> stuff=Permissions.getAllowedValues(user, "xnat:projectData", "xnat:projectData/ID", SecurityManager.READ );
			for (Object id : stuff) {
				String proj=(String)id;
				if(proj!=null){
					Study study=this.findById(user,proj);
					//fix this.
					if(study!=null && StringUtils.equals(TYPE, study.getType()) && (StringUtils.equals(PUBLIC,study.getAccessibility())||StringUtils.equals(PROTECTED,study.getAccessibility()))){
						studies.add(study);
					}
				}
			}
		}catch(Exception ex){
			logger.warn("findReadableByUser: no properties found");
		}
		return studies;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * @see org.nrg.xnat.services.project.properties.ProjectPropertiesService#findBy(java.lang.String)
	 */
	public Study findById(UserI user,String project) throws StudyException{
		try{
			return this.loadStudy(project, user);
		}catch(Exception ex){
			logger.warn("can't find project"+project);
			//throw new StudyException(ex.getMessage());
		}
		return null;
	}
	
	Study loadStudy(String project,UserI user) throws InvalidItemException, Exception{
		XnatProjectdata  xproj1 = XnatProjectdata.getProjectByIDorAlias(project, user, false);
		
		if(!xproj1.canRead(user)) throw new Exception("Not Authorized");//should be a Runtime NotAuthorized exception.
		
		Study study=new Study();
		study.setId(xproj1.getId());
		study.setName(xproj1.getName());
		study.setType(xproj1.getType());
		study.setDescription(xproj1.getDescription());
		study.setSecondaryID(xproj1.getSecondaryId());
		
		study.setInsertDate(xproj1.getInsertDate());
		//pull out the keywords
		if(StringUtils.isNotEmpty(xproj1.getKeywords())){
			StringTokenizer tokenizer=new StringTokenizer(xproj1.getKeywords(),",");
			List<String> keywords=new ArrayList<String>();
			while (tokenizer.hasMoreElements()) {
				keywords.add((String)tokenizer.nextElement());
			}
			study.setKeywords(keywords);
		}
		
		
		List<XnatProjectdataAlias> aliases=xproj1.getAliases_alias();
		List<String> studyaliases=new ArrayList<String>();
		for (XnatProjectdataAlias alias : aliases) {
			studyaliases.add(alias.getAlias());
		}
		study.setAliases(studyaliases);
		
		
		study.setPi(investigatorService.getPI(user, project));
		study.setInvestigators(investigatorService.getInvestigatorsForProject(user, project));
		
		
		//adding publication support
		List<Publication> publications=publicationService.getPublicationsForProject(project);
		study.setPublications(publications);
		
	
		
		List<XnatProjectdataField> fields =xproj1.getFields_field();
		Map<String,Object> properties=new HashMap<String,Object>();
		for (XnatProjectdataField field : fields) {
			//add code to convert boolean and integers!!!
			properties.put(field.getName(), convertField(field.getField()));
		}
		study.setProperties(properties);
		
		
		study.setAccessibility(xproj1.getPublicAccessibility());
		return study;
	}
	
	public Object convertField(String field){
		if("true".equals(field)){
			return Boolean.TRUE;
		}
		if("false".equals(field)){
			return Boolean.FALSE;
		}
		if(StringUtils.isNumeric(field)){
			return Integer.valueOf(field);
		}
		return field;
		}
	
	public  String toJson(Study study) throws StudyException {

	    try {
			return  MAPPER.writeValueAsString(study);
		} catch (JsonProcessingException e) {
			logger.warn(e.getMessage());
			throw new StudyException(e.getMessage());
			
		}


	}
	
	public  Study fromJson(String serializedString)  throws StudyException{
		try{		    
	    	Study study = MAPPER.readValue(serializedString, Study.class);
	    	return study;
		} catch (JsonProcessingException e) {
			logger.warn(e.getMessage());
			throw new StudyException(e.getMessage());
		} catch (IOException e) {
			logger.warn(e.getMessage());
			throw new StudyException(e.getMessage());
		}

	}

	/*
	 * How to filter study data from properties. work in progress.. stopped, all on client side via javascript. Simply
	 * return all projects with user access.
	 * 
	 * (non-Javadoc)
	 * @see org.nrg.xnat.services.project.properties.ProjectPropertiesService#filter(org.nrg.xnat.services.project.properties.SearchProperties)
	 */
	//@Override
	//TODO parse json array
	public List<Study> filter(UserI user) throws ConfigServiceException ,StudyException{
		//filter out non studies...

		List<Study> props=this.findReadableByUser(user);
		return props;
	}
	
	public StudyInvestigator create(StudyInvestigator investigator){
		return null;
	}
	
	public Study create(Study study){
		//see project creation. 
		return null;
		
	}


	@Override
	public StudySummary getStudySummary() {
		StudySummary summary=new StudySummary();
		summary.setStudyCount(this.getCount());
		summary.setTotalHealthySubjectCount(this.getHealthy());
		summary.setTotalSubjectCount(this.getTotalSubjects());
		return summary;
	}
	
	public Integer getCount() {
		try {

			String query = "SELECT COUNT(proj.id) FROM xdat_user u JOIN xdat_element_access xea ON u.xdat_user_id=xea.xdat_user_xdat_user_id JOIN xdat_field_mapping_set xfms ON xea.xdat_element_access_id=xfms.permissions_allow_set_xdat_elem_xdat_element_access_id JOIN xdat_field_mapping xfm ON xfms.xdat_field_mapping_set_id=xfm.xdat_field_mapping_set_xdat_field_mapping_set_id AND xfm.field='xnat:projectData/ID' AND read_element=1 JOIN xnat_projectdata proj ON xfm.field_value=proj.id AND proj.type='study' WHERE u.login='guest' ";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(XDAT.getDataSource());
			int count = jdbcTemplate.queryForObject(query,Integer.class);

			logger.debug(query);
			
			return new Integer(count);
		} catch (Exception e) {
			logger.error("failed to find study count", e);
			return 0;
			//throw new RuntimeException();
		}

	}
	public Integer getTotalSubjects() {
		try {

			String query = "select sum(CAST(coalesce(field, '0') AS integer)) FROM ( SELECT DISTINCT proj.id, field.field from xnat_projectData_field field JOIN xnat_projectData proj ON field.fields_field_xnat_projectdata_id=proj.id JOIN xdat_field_mapping xfm ON proj.id=xfm.field_value JOIN xdat_field_mapping_set xfms ON xfm.xdat_field_mapping_set_xdat_field_mapping_set_id=xfms.xdat_field_mapping_set_id AND xfm.field='xnat:projectData/ID' AND read_element=1 JOIN xdat_element_access xea ON xfms.permissions_allow_set_xdat_elem_xdat_element_access_id=xea.xdat_element_access_id JOIN xdat_user u ON xea.xdat_user_xdat_user_id=u.xdat_user_id AND u.login='guest' where field.name  = 'dpuk:expected_total_subject_count' ORDER BY proj.id ) SRCH";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(XDAT.getDataSource());
			int count = jdbcTemplate.queryForObject(query,Integer.class);

			logger.debug(query);
			
			return count;
		} catch (Exception e) {
			logger.error("failed to find dpuk:expected_total_subject_count", e);
			return 0;
			//throw new RuntimeException();
		}

	}
	public Integer getHealthy() {
		try {
			String query = "select sum(CAST(coalesce(field, '0') AS integer)) FROM ( SELECT DISTINCT proj.id, field.field from xnat_projectData_field field JOIN xnat_projectData proj ON field.fields_field_xnat_projectdata_id=proj.id JOIN xdat_field_mapping xfm ON proj.id=xfm.field_value JOIN xdat_field_mapping_set xfms ON xfm.xdat_field_mapping_set_xdat_field_mapping_set_id=xfms.xdat_field_mapping_set_id AND xfm.field='xnat:projectData/ID' AND read_element=1 JOIN xdat_element_access xea ON xfms.permissions_allow_set_xdat_elem_xdat_element_access_id=xea.xdat_element_access_id JOIN xdat_user u ON xea.xdat_user_xdat_user_id=u.xdat_user_id AND u.login='guest' where field.name  = 'dpuk:expected_healthy_control_count' ORDER BY proj.id ) SRCH";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(XDAT.getDataSource());
			int count = jdbcTemplate.queryForObject(query,Integer.class);
			logger.debug(query);
			return count;
		} catch (Exception e) {
			logger.error("failed to find dpuk:expected_healthy_control_count", e);
			return 0;
			//throw new RuntimeException();
		}

	}
}
