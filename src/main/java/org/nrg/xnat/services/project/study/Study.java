package org.nrg.xnat.services.project.study;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.services.investigator.Investigator;

public class Study implements Serializable{

	private String id;
	private String name;
	private String type;
	private String description;
	private List<String> keywords;
	private String secondaryID;
	private Date insertDate;
	private String accessibility;
	
	private Map<String, Object> properties;
	public Map<String, Object> getProperties() {
		return properties;
	}


	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	//private List<StudyProperty> properties;
	private Investigator pi;
	
	private List<Investigator> investigators;
	private List<String> aliases;
	
	private List<Publication> publications;
	/**
	 * 
	 */
	private static final long serialVersionUID = -3868833917940824078L;
	
	
	public List<Publication> getPublications() {
		return publications;
	}


	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	

	public List<Investigator> getInvestigators() {
		return investigators;
	}


	public void setInvestigators(List<Investigator> investigators) {
		this.investigators = investigators;
	}


	public List<String> getAliases() {
		return aliases;
	}


	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Investigator getPi() {
		return pi;
	}


	public void setPi(Investigator pi) {
		this.pi = pi;
	}


	public void setDescription(String description) {
		this.description=description;
	}
	public String getDescription() {
		return description;
	}


	public void setKeywords(List<String> keywords) {
		this.keywords=keywords;
		
	}
	public List<String> getKeywords() {
		return keywords;
	}


	public String getSecondaryID() {
		return secondaryID;
	}


	public void setSecondaryID(String secondaryID) {
		this.secondaryID = secondaryID;
	}


	public Date getInsertDate() {
		return insertDate;
	}


	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}


	public String getAccessibility() {
		return accessibility;
	}


	public void setAccessibility(String accessibility) {
		this.accessibility = accessibility;
	}

}
