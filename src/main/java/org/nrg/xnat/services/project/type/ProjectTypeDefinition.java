package org.nrg.xnat.services.project.type;

public  interface ProjectTypeDefinition {
	
		public String getDescription();
		public String getName();
		public String getKey();
			
		

		
	
}
