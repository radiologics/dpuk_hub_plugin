package org.nrg.xnat.services.project.type;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.xdat.XDAT;

public class ProjectType {
	static Logger logger = Logger.getLogger(ProjectType.class);
	private static ProjectTypeService projectTypeService = null;

	public static ProjectTypeService getProjectTypeService() {
		if (projectTypeService == null) {
			// default to ProjectTypeServiceImpl implementation (unless a
			// different default is configured)
			// we can swap in other ones later by setting a default
			// we can even have a config tab in the admin ui which allows sites
			// to select their configuration of choice.
			try {
				String className = XDAT
						.getSiteConfigurationProperty(
								"org.nrg.xnat.services.project.type.ProjectTypeService.default",
								"org.nrg.xnat.services.project.type.ProjectTypeServiceImpl");
				projectTypeService = (ProjectTypeService) Class.forName(
						className).newInstance();
			} catch (ClassNotFoundException e) {
				logger.error("", e);
			} catch (InstantiationException e) {
				logger.error("", e);
			} catch (IllegalAccessException e) {
				logger.error("", e);
			} catch (ConfigServiceException e) {
				logger.error("", e);
			}
		}
		return projectTypeService;
	}

	public static Collection<ProjectTypeDefinition> getProjectTypes() {
		return getProjectTypeService().getProjectTypes();
	}
}