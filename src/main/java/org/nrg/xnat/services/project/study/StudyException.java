package org.nrg.xnat.services.project.study;

/*
 * org.nrg.xnat.services.project.study.StudyException.
 * XNAT http://www.xnat.org
 * Copyright (c) 2015, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 */
import java.util.ArrayList;
import java.util.List;
import org.nrg.config.entities.Configuration;

public class StudyException extends Throwable {
	
	static final long serialVersionUID = 6690308402621986012L;
	
	public final List<Configuration> problem_scripts = new ArrayList<Configuration>();

	public StudyException() {
	}
	
	public StudyException(String message) {
		super(message);
	}

	public StudyException(Throwable cause) {
		super(cause);
	}

	public StudyException(String message, Throwable cause){
		super(message, cause);
	}

	public StudyException(String message, List<Configuration> ss) {
		super(message);
		this.problem_scripts.addAll(ss);
	}

	public StudyException(Throwable cause, List<Configuration> ss) {
		super(cause);
		this.problem_scripts.addAll(ss);
	}

	public StudyException(String message, Throwable cause, List<Configuration> ss) {
		super(message, cause);
		this.problem_scripts.addAll(ss);
	}
}
