package org.nrg.xnat.services.project.relationship;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectRelationshipServiceImpl
		extends
		AbstractHibernateEntityService<ProjectRelationship, ProjectRelationshipDao>
		implements ProjectRelationshipService {

	//should be final aagh
	private  ProjectRelationshipDao projectRelationshipDao;

	//CGLIB-based proxy require default constuctor
	public ProjectRelationshipServiceImpl() {
	} 
	
	@Autowired
	public ProjectRelationshipServiceImpl(ProjectRelationshipDao projectRelationshipDao) {
		this.projectRelationshipDao = projectRelationshipDao;
	} 

	static Logger logger = Logger
			.getLogger(ProjectRelationshipServiceImpl.class);
	
	

	@Override
	protected ProjectRelationshipDao getDao() {
		return projectRelationshipDao;
	}

	@Override
	@Transactional
	public List<ProjectRelationship> findByExample(
			ProjectRelationship projectRelationship) {
		return getDao().findByExample(projectRelationship);
	}

	@Override
	@Transactional
	public List<ProjectRelationship> findAllBy(String project1,
			String project2, String type) {
		return getDao().findAllBy(project1, project2, type);
	}

	@Override
	@Transactional
	public ProjectRelationship addOrUpdateRelationship(
			ProjectRelationship projectRelationship) {
		ProjectRelationship pr = getDao().findBy(
				projectRelationship.getProject1(),
				projectRelationship.getProject2(),
				projectRelationship.getType());
		if (pr != null) {
			if (!StringUtils.equals(projectRelationship.getProperties(),
					pr.getProperties())) {
				pr.setProperties(projectRelationship.getProperties());
			}
			getDao().update(pr);
			if (logger.isDebugEnabled()) {
				logger.debug("update relationship " + pr.getProject1() + " : "
						+ pr.getProject2() + " : " + pr.getType());
			}
			return pr;
		} else {
			ProjectRelationship nPr = newEntity();
			nPr.setProject1(projectRelationship.getProject1());
			nPr.setProject2(projectRelationship.getProject2());
			nPr.setType(projectRelationship.getType());
			nPr.setProperties(projectRelationship.getProperties());
			getDao().create(nPr);
			if (logger.isDebugEnabled()) {
				logger.debug("create relationship " + nPr.getProject1() + " : "
						+ nPr.getProject2() + " : " + nPr.getType());
			}
			return nPr;
		}

	}

	@Override
	@Transactional
	public ProjectRelationship findById(Long id) {
		return getDao().findById(id);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ProjectRelationship pr = getDao().findById(id);
		if (pr != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Deleting project relationship: "
						+ pr.getProject1() + " " + pr.getProject2() + " "
						+ pr.getType());
			}
			getDao().delete(pr);

		}
	}

	@Override
	@Transactional
	public void delete(String proj1, String proj2, String type) {
		ProjectRelationship pr = getDao().findBy(proj1, proj2, type);
		try {
			if (pr != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Deleting project relationship: "
							+ pr.getProject1() + " " + pr.getProject2() + " "
							+ pr.getType());
				}
				this.getDao().delete(pr);
			} else {
				logger.warn("Not Deleting project relationship: relationship does not exist");

			}
		} catch (Exception e) {
			logger.warn("Not Deleting project relationship: "
					+ pr.getProject1() + " " + pr.getProject2() + " "
					+ pr.getType());
			e.printStackTrace();
		}
	}
}
