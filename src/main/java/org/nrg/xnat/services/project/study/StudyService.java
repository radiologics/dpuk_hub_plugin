package org.nrg.xnat.services.project.study;

import java.util.List;

import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.xft.security.UserI;

public interface StudyService{
	public Study findById(UserI user,String project)throws StudyException;
	public List<Study> filter(UserI user) throws ConfigServiceException ,StudyException;
	public List<Study> findReadableByUser(UserI user) throws StudyException;
	public List<Study> findEditableByUser(UserI user) throws StudyException;
	public  String toJson(Study study) throws StudyException;
	public  Study fromJson(String serializedString)  throws StudyException;
	public StudySummary getStudySummary();
}
