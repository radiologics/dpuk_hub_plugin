package org.nrg.xnat.services.project.relationship;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dcm4che2.iod.module.composite.RelatedSeries;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectRelationshipDaoImpl extends
		AbstractHibernateDAO<ProjectRelationship> implements
		ProjectRelationshipDao {
	static Logger logger = Logger.getLogger(ProjectRelationshipDaoImpl.class);

	public List<ProjectRelationship> findByExample(
			ProjectRelationship projectRelationship) {
		projectRelationship.setEnabled(Boolean.TRUE);
		return super.findByExample(projectRelationship, null);
	}

	public List<ProjectRelationship> findByProject(String project) {
		Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project1", project));
		criteria.add(Restrictions.eq("enabled", Boolean.TRUE));
		return (List<ProjectRelationship>) criteria.list();
	}

	public ProjectRelationship findBy(String project1, String project2,
			String type) {
		Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project1", project1));
		criteria.add(Restrictions.eq("project2", project2));
		criteria.add(Restrictions.eq("type", type));
		criteria.add(Restrictions.eq("enabled", Boolean.TRUE));
		if (criteria.list().size() == 0) {
			return null;
		}
		return (ProjectRelationship) criteria.list().get(0);
	}

	public List<ProjectRelationship> findAllBy(String project1,
			String project2, String type) {
	
		Criteria criteria = getCriteriaForType();
		if(StringUtils.isNotEmpty(project1))
			criteria.add(Restrictions.eq("project1", project1));
		if(StringUtils.isNotEmpty(project2))
			criteria.add(Restrictions.eq("project2", project2));
		if(StringUtils.isNotEmpty(type))
			criteria.add(Restrictions.eq("type", type));
		criteria.add(Restrictions.eq("enabled", Boolean.TRUE));

		return (List<ProjectRelationship>) criteria.list();
	}

	

	public void delete(Long id) {
		ProjectRelationship pr = super.findById(id);
		super.delete(pr);
	}


}
