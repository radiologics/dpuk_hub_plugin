package org.nrg.xnat.services.project.relationship;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.nrg.framework.orm.hibernate.annotations.Auditable;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Auditable
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "project1",
		"project2", "type", "disabled" }))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
public class ProjectRelationship extends AbstractHibernateEntity implements
		ProjectRelationshipI {
	
	//complete overkill, but you never know.
	public static final int MAX_FILE_LENGTH = 1048576 ; //max size for postgres, see htup.h #define MaxAttrSize (10 * 1024 * 1024)

	/**
	 * 
	 */
	private static final long serialVersionUID = 511847952572889112L;

	String project1;
	String project2;
	String type;
	String properties;

	public String getProject1() {
		return project1;
	}

	public void setProject1(String project1) {
		this.project1 = project1;
	}

	public String getProject2() {
		return project2;
	}

	public void setProject2(String project2) {
		this.project2 = project2;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(length=MAX_FILE_LENGTH)
	public String getProperties() {
		return properties;
	}
	

	public void setProperties(String properties) {
		this.properties = properties;
	}

}
