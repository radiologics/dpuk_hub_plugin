package org.nrg.xnat.services.project.relationship;

public interface ProjectRelationshipI {
	public long getId();
	public String getProject1();
	public String getProject2();
	public String getType();
	public String getProperties();

	
}
