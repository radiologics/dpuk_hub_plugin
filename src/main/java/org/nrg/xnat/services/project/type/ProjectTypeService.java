package org.nrg.xnat.services.project.type;

import java.util.Collection;


public interface ProjectTypeService {
	
	
	public Collection<ProjectTypeDefinition> getProjectTypes();
}
