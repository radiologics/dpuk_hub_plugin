package org.nrg.xnat.services.project.relationship;

import java.util.List;

import org.nrg.framework.orm.hibernate.BaseHibernateDAO;

public interface ProjectRelationshipDao extends BaseHibernateDAO<ProjectRelationship>{
	public List<ProjectRelationship> findByExample(ProjectRelationship projectRelationship);
	public ProjectRelationship findBy(String project1,String project2,String type);
	public List<ProjectRelationship> findAllBy(String project1,String project2,String type);
	List<ProjectRelationship> findByProject(String project);
}
