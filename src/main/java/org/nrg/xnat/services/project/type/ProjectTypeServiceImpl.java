package org.nrg.xnat.services.project.type;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;
import org.nrg.xft.XFT;
import org.nrg.xdat.turbine.utils.PropertiesHelper;

public class ProjectTypeServiceImpl implements ProjectTypeService {

	private static final String DEFAULT_PROPERTIES_FILE = "META-INF/configuration/default-projecttype-definition.properties";
	private static final String PROJECTTYPE_DEFINITION_PROPERTIES = "-projecttype-definition.properties";
	private static final String NAME = "name";
	private static final String DESC = "description";
	private static final String KEY = "key";
	
	private static final String[] PROP_OBJECT_FIELDS = new String[] { NAME, DESC, KEY };
	private static final String PROP_OBJECT_IDENTIFIER = "org.nrg.ProjectType";

	public static Collection<ProjectTypeDefinition> allProjectTypes = null;
	
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ProjectTypeServiceImpl.class);
	
	public ProjectTypeServiceImpl() {
		init();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void init() {
		if (allProjectTypes == null) {
			allProjectTypes = new TreeSet<ProjectTypeDefinition>(
					new Comparator() {
						@Override
						public int compare(Object o1, Object o2) {
							return ((ProjectTypeDefinition) o1).getKey().compareTo(((ProjectTypeDefinition) o2).getKey());
						}
					});
			
			// Load the default project type definitions file
			getDefaultProjectTypeDefinitions();
			
			// Load any custom project type definitions from the XNAT configuration directory (usually webapps/ROOT/WEB-INF/conf)
			getProjectTypeDefinitionsFromDirectory(XFT.GetConfDir());

		}
	}

	@Override
	public Collection<ProjectTypeDefinition> getProjectTypes() {
		return allProjectTypes;
	}
	
	private void getDefaultProjectTypeDefinitions() {
		getProjectTypeDefinitionsFromResource(ProjectTypeServiceImpl.class.getClassLoader().getResource(DEFAULT_PROPERTIES_FILE));
	}
	
	private void getProjectTypeDefinitionsFromResource(URL propertiesFile) {
		final Map<String, Map<String, Object>> types = PropertiesHelper.RetrievePropertyObjects(propertiesFile,PROP_OBJECT_IDENTIFIER, PROP_OBJECT_FIELDS);
		for (Map<String, Object> type : types.values()) {
			allProjectTypes.add(getProjectTypeDefinition(type));
		}
	}
	
	private void getProjectTypeDefinitionsFromDirectory(String directory) {
		File[] propFiles = new File(directory).listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(PROJECTTYPE_DEFINITION_PROPERTIES);
					}
				});
		
		if (propFiles != null) {
			for (File props : propFiles) {
				final Map<String, Map<String, Object>> types = PropertiesHelper.RetrievePropertyObjects(props,PROP_OBJECT_IDENTIFIER, PROP_OBJECT_FIELDS);
				for (Map<String, Object> type : types.values()) {
					allProjectTypes.add(getProjectTypeDefinition(type));
				}
			}
		}
	}
	
	private ProjectTypeDefinition getProjectTypeDefinition(Map<String, Object> type) {
		ProjectTypeDefinitionImpl def = new ProjectTypeDefinitionImpl();
		def.setKey((String) type.get(KEY));
		def.setName(type.get(NAME).toString());
		def.setDescription(type.get(DESC).toString());
		return def;
	}
}
