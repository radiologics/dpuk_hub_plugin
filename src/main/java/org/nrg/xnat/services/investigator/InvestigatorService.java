package org.nrg.xnat.services.investigator;

import java.io.IOException;
import java.util.List;
import org.nrg.xft.security.UserI;


/**
 * Created by james@radiologics on 10/5/2015.
 */
public interface InvestigatorService {
    public static String SERVICE_NAME = "InvestigatorService";

	public List<Investigator> getInvestigatorsForProject(UserI user,String project);

	public Investigator getInvestigator(UserI user,String id, String project);
    public String toJson(Investigator investigator) throws IOException;
    public String toJson(final List<Investigator> investigators) throws IOException;
	public void delete(UserI user, String id, String project) throws Exception;
	public void add(UserI user, String id, String project) throws Exception;
	public void addPI(UserI user, String id, String project) throws Exception;
	public void deletePI(UserI user, String project) throws Exception;

	public Investigator getPI(UserI user,String project);


}
