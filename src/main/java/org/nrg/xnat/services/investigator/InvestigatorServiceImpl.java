package org.nrg.xnat.services.investigator;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.nrg.xdat.model.XnatInvestigatordataI;
import org.nrg.xdat.om.XnatInvestigatordata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.utils.WorkflowUtils;
import org.springframework.stereotype.Service;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Service
public class InvestigatorServiceImpl  implements InvestigatorService {

	
	static Logger logger = Logger.getLogger(InvestigatorServiceImpl.class);
	
	private final ObjectMapper _mapper = new ObjectMapper() {
		private static final long serialVersionUID = -8755999975808287368L;
	{
        this.disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
        this.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        this.enable(DeserializationFeature.WRAP_EXCEPTIONS);
        this.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        this.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }};
	
	
    @Override
	public void add(UserI user, String id, String project) throws Exception {
		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
		
		XnatInvestigatordata inv=XnatInvestigatordata.getXnatInvestigatordatasByXnatInvestigatordataId(id, user, true);
		//xproj.getInvestigators_investigator().add(inv);
		//xproj.setId(inv);
		//xproj.add(inv);
		xproj.addInvestigators_investigator(inv);
		
        final PersistentWorkflowI wrk = PersistentWorkflowUtils.getOrCreateWorkflowData(null, user, XnatProjectdata.SCHEMA_ELEMENT_NAME, xproj.getId(), xproj.getId(), EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN,EventUtils.TYPE.PROCESS,"Add Investigator"));
		final EventMetaI ci = wrk.buildEvent();
		try {
			SaveItemHelper.authorizedSave(xproj, user, false, true, ci);
		} catch (Exception e) {
			WorkflowUtils.fail(wrk, ci);
			e.printStackTrace();
		}
		return;
	}
    
    @Override
   	public void addPI(UserI user, String id, String project) throws Exception {
   		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
   		
   		XnatInvestigatordataI inv=XnatInvestigatordata.getXnatInvestigatordatasByXnatInvestigatordataId(id, user, true);
   		
   		xproj.setPi(inv);
   		
        final PersistentWorkflowI wrk = PersistentWorkflowUtils.getOrCreateWorkflowData(null, user, XnatProjectdata.SCHEMA_ELEMENT_NAME, xproj.getId(), xproj.getId(), EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN,EventUtils.TYPE.PROCESS,"Add Principal Investigator"));
   		final EventMetaI ci = wrk.buildEvent();
   		try {
   			SaveItemHelper.authorizedSave(xproj, user, false, true, ci);
   		} catch (Exception e) {
   			WorkflowUtils.fail(wrk, ci);
   			e.printStackTrace();
   		}
   		return;
   	}
	
    
    
	@Override
	public List<Investigator> getInvestigatorsForProject(UserI user,String project) {
		List<Investigator> invs=new ArrayList<Investigator>();

		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
		if(xproj!=null){
			List<XnatInvestigatordata> investigators=xproj.getInvestigators_investigator();
			for (XnatInvestigatordataI xnatInvestigatordata : investigators) {
				Investigator investigator=new Investigator();
				try {
					BeanUtils.copyProperties(investigator, xnatInvestigatordata);
					investigator.setXnat_investigatordata_id(xnatInvestigatordata.getXnatInvestigatordataId());
					invs.add(investigator);
					
				} catch (IllegalAccessException | InvocationTargetException e) {
					logger.info(e.getMessage());
				}
			}
		}
		return invs;
	}
	
	
	@Override
    public String toJson(final List<Investigator> investigators) throws IOException{
        return _mapper.writeValueAsString(investigators);
    }
	
	@Override
    public String toJson(final Investigator investigator) throws IOException{
        return _mapper.writeValueAsString(investigator);
    }
	
	@Override
	public Investigator getInvestigator(UserI user,String id, String project) {
		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
		if(xproj!=null){
			List<XnatInvestigatordata> investigators=xproj.getInvestigators_investigator();
			for (XnatInvestigatordata xnatInvestigatordata : investigators) {
				try {
					Investigator investigator=new Investigator();
					if(Integer.valueOf(id).equals(xnatInvestigatordata.getXnatInvestigatordataId())){
						BeanUtils.copyProperties(investigator, xnatInvestigatordata);
						investigator.setXnat_investigatordata_id(xnatInvestigatordata.getXnatInvestigatordataId());
						return investigator;
					}
				} catch (IllegalAccessException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					logger.info(e.getMessage());
				}
			}
		}
		
		return null;
	}

	@Override
	public void delete(UserI user, String id, String project) throws Exception{
		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
		XnatInvestigatordata inv=XnatInvestigatordata.getXnatInvestigatordatasByXnatInvestigatordataId(id, user, true);
		xproj.getInvestigators_investigator().remove(inv);
		//save...
        final PersistentWorkflowI wrk = PersistentWorkflowUtils.getOrCreateWorkflowData(null, user, XnatProjectdata.SCHEMA_ELEMENT_NAME, xproj.getId(), xproj.getId(), EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN,EventUtils.TYPE.PROCESS,"Remove Investigator"));
		final EventMetaI ci = wrk.buildEvent();
		try {
			SaveItemHelper.authorizedRemoveChild(xproj.getItem(), "xnat:projectData/investigators/investigator", inv.getItem(), user, ci);
		} catch (Exception e) {
			WorkflowUtils.fail(wrk, ci);
			e.printStackTrace();
		}
	}

	@Override
	public void deletePI(UserI user, String project) throws Exception{
		XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
		xproj.removePi();
		//save...
        final PersistentWorkflowI wrk = PersistentWorkflowUtils.getOrCreateWorkflowData(null, user, XnatProjectdata.SCHEMA_ELEMENT_NAME, xproj.getId(), xproj.getId(), EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN,EventUtils.TYPE.PROCESS,"Remove Principal Investigator"));
		final EventMetaI ci = wrk.buildEvent();
		try {
   			SaveItemHelper.authorizedSave(xproj, user, false, true, ci);
		} catch (Exception e) {
			WorkflowUtils.fail(wrk, ci);
			e.printStackTrace();
		}
	}


	@Override
	public Investigator getPI(UserI user,String project) {
			XnatProjectdata xproj=XnatProjectdata.getProjectByIDorAlias(project, user, true);
			if(xproj!=null){
				if (xproj.getPi()!=null){
						Investigator investigator = new Investigator();
						XnatInvestigatordata pi=xproj.getPi();
						try {
							BeanUtils.copyProperties(investigator, pi);
							investigator.setXnat_investigatordata_id(pi.getXnatInvestigatordataId());	
							return investigator;
						} catch (IllegalAccessException
								| InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
			}
			return null;
	}

    
    
    

}
