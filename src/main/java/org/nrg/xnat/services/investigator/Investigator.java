package org.nrg.xnat.services.investigator;

import java.io.Serializable;


//stepping stone to entity.
public class Investigator implements Serializable{

	private static final long serialVersionUID = -8148718560492379869L;
	private String title ;
	private String firstname; 
	private String lastname  ;
	private String institution ;    
	private String department;
	private String email  ;  
	private String phone;    

	private String id;   
	//private String investigatordata_info; 
	private Integer xnat_investigatordata_id;
	
	
	
	
	public Integer getXnat_investigatordata_id() {
		return xnat_investigatordata_id;
	}
	public void setXnat_investigatordata_id(Integer xnat_investigatordata_id) {
		this.xnat_investigatordata_id = xnat_investigatordata_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	

}
