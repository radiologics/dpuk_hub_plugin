package org.nrg.xnat.services.search;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xnat.services.search.SubjectSummaryStatsService.CriteriaField;
import org.nrg.xnat.services.search.SubjectSummaryStatsService.FIELD_TYPE;

import com.google.common.collect.Lists;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to support real time querying of pre-defined subject fields as summary scores.
 * 
 * This is a basic implementation of the Subject Summary Stats interface used for DPUK data.
 *
 */ 
public class BasicDPUKSummaryStatsImpl implements SubjectSummaryStatsI {
	private static final Log _log = LogFactory.getLog(BasicDPUKSummaryStatsImpl.class);
	
	private static final String BASE_QUERY="SELECT study.secondary_id AS study, study.id AS study_pk, pfreeze.secondary_id as project_display ,  CASE WHEN total_diagnosis<2 OR total_diagnosis IS NULL THEN 'Unknown'  WHEN ((diagnosis_ad = 0 OR diagnosis_ad IS NULL) AND (diagnosis_pd = 0 OR diagnosis_pd IS NULL) AND (diagnosis_vad = 0 OR diagnosis_vad IS NULL) AND (diagnosis_mci = 0 OR diagnosis_mci IS NULL) AND (diagnosis_ftd = 0 OR diagnosis_ftd IS NULL) AND (diagnosis_lbd = 0 OR diagnosis_lbd IS NULL)) AND ((followup_ad = 0 OR followup_ad IS NULL) AND (followup_pd = 0 OR followup_pd IS NULL) AND (followup_vad = 0 OR followup_vad IS NULL) AND (followup_mci = 0 OR followup_mci IS NULL) AND (followup_ftd = 0 OR followup_ftd IS NULL) AND (followup_lbd = 0 OR followup_lbd IS NULL)) THEN 'Normal - Normal'  WHEN ((diagnosis_ad = 0 OR diagnosis_ad IS NULL) AND (diagnosis_pd = 0 OR diagnosis_pd IS NULL) AND (diagnosis_vad = 0 OR diagnosis_vad IS NULL) AND (diagnosis_mci = 0 OR diagnosis_mci IS NULL) AND (diagnosis_ftd = 0 OR diagnosis_ftd IS NULL) AND (diagnosis_lbd = 0 OR diagnosis_lbd IS NULL)) AND followup_mci>0 THEN 'Normal - MCI'  WHEN ((diagnosis_ad = 0 OR diagnosis_ad IS NULL) AND (diagnosis_pd = 0 OR diagnosis_pd IS NULL) AND (diagnosis_vad = 0 OR diagnosis_vad IS NULL) AND (diagnosis_mci = 0 OR diagnosis_mci IS NULL) AND (diagnosis_ftd = 0 OR diagnosis_ftd IS NULL) AND (diagnosis_lbd = 0 OR diagnosis_lbd IS NULL)) AND (followup_vad>0 OR followup_ftd>0 OR followup_lbd>0 OR followup_ad>0) THEN 'Normal - Dementia'  WHEN diagnosis_mci>0 AND followup_mci>0 THEN 'MCI - MCI'  WHEN diagnosis_mci>0 AND (followup_vad>0 OR followup_ftd>0 OR followup_lbd>0 OR followup_ad>0) THEN 'MCI - Dementia'  ELSE 'Unknown'  END AS conversion,  sub.id, sub.project, AGE.age, CASE LOWER(demo.gender) WHEN 'male' THEN 'Male' WHEN 'female' THEN 'Female' WHEN 'f' THEN 'Female' WHEN 'm' THEN 'Male' ELSE demo.gender END AS gender, CASE LOWER(demo.handedness) WHEN 'left' THEN 'Left' WHEN 'right' THEN 'Right' WHEN 'ambidextrous' THEN 'Ambidextrous' WHEN 'r' THEN 'Right' WHEN 'l' THEN 'Left' WHEN 'a' THEN 'Ambidextrous' ELSE demo.handedness END AS handedness, education,  CASE risks.has_familial_gene WHEN 0 THEN 'Negative' WHEN 1 THEN 'Positive' ELSE NULL END AS has_familial_gene,   CASE risks.has_other_risk_gene WHEN 0 THEN 'Negative' WHEN 1 THEN 'Positive' ELSE NULL END AS has_other_risk_gene,  CASE risks.has_other_risk WHEN 0 THEN 'Negative' WHEN 1 THEN 'Positive' ELSE NULL END AS has_other_risk,apoe,  diagnosis_ad, diagnosis_pd, diagnosis_vad, diagnosis_mci, diagnosis_ftd, diagnosis_lbd, ((diagnosis_ad = 0 OR diagnosis_ad IS NULL) AND (diagnosis_pd = 0 OR diagnosis_pd IS NULL) AND (diagnosis_vad = 0 OR diagnosis_vad IS NULL) AND (diagnosis_mci = 0 OR diagnosis_mci IS NULL) AND (diagnosis_ftd = 0 OR diagnosis_ftd IS NULL) AND (diagnosis_lbd = 0 OR diagnosis_lbd IS NULL)) diagnosis_normal,   CASE WHEN mmse<=6 THEN '0-6' WHEN mmse >=7 AND mmse <=12 THEN '7-12' WHEN mmse >=13 AND mmse <=18 THEN '13-18' WHEN mmse >=19 AND mmse <=24 THEN '19-24' WHEN mmse >=25 AND mmse <=30 THEN '25-30' END AS mmse,  CASE WHEN moca<=6 THEN '0-6' WHEN moca >=7 AND moca <=12 THEN '7-12' WHEN moca >=13 AND moca <=18 THEN '13-18' WHEN moca >=19 AND moca <=24 THEN '19-24' WHEN moca >=25 AND moca <=30 THEN '25-30' END AS moca,   cdr, fdg, c11, f18, tau, other_pet, task_MEG, rest_MEG, other_meg, T1_3D, T2_3D, FLAIR_3D, T1_2D, T2_2D, FLAIR_2D, T2_STAR, SWI, REST_MR, TASK_MR, DTI, ASL, OTHER_MR,    CASE (fdg > 1 OR c11 > 1 OR f18 > 1 OR tau > 1 OR other_pet > 1 OR task_MEG > 1 OR rest_MEG > 1 OR other_meg > 1 OR T1_3D > 1 OR T2_3D > 1 OR FLAIR_3D > 1 OR T1_2D > 1 OR T2_2D > 1 OR FLAIR_2D > 1 OR T2_STAR > 1 OR SWI > 1 OR REST_MR > 1 OR TASK_MR > 1 OR DTI > 1 OR ASL >1) WHEN true THEN true ELSE false END  AS LONGITUDNAL_IMG ,   followup_ad, followup_pd, followup_vad, followup_mci, followup_ftd, followup_lbd, ((followup_ad = 0 OR followup_ad IS NULL) AND (followup_pd = 0 OR followup_pd IS NULL) AND (followup_vad = 0 OR followup_vad IS NULL) AND (followup_mci = 0 OR followup_mci IS NULL) AND (followup_ftd = 0 OR followup_ftd IS NULL) AND (followup_lbd = 0 OR followup_lbd IS NULL)) AS followup_normal, diagnosis_followups, total_diagnosis,   (followup_ad IS NOT NULL AND followup_pd IS NOT NULL AND followup_vad IS NOT NULL AND followup_mci IS NOT NULL AND followup_ftd IS NOT NULL AND followup_lbd IS NOT NULL) AS LONG_CLIN  FROM xnat_subjectData sub  JOIN xnat_projectData pfreeze ON sub.project=pfreeze.id  JOIN xhbm_project_relationship rel ON pfreeze.id =rel.project2 AND rel.type='active_freeze' AND rel.enabled=true  JOIN (       SELECT DISTINCT proj.id, proj.secondary_id        FROM xdat_user u        JOIN xdat_element_access xea ON u.xdat_user_id=xea.xdat_user_xdat_user_id         JOIN xdat_field_mapping_set xfms ON xea.xdat_element_access_id=xfms.permissions_allow_set_xdat_elem_xdat_element_access_id  JOIN xdat_field_mapping xfm ON xfms.xdat_field_mapping_set_id=xfm.xdat_field_mapping_set_xdat_field_mapping_set_id AND xfm.field='xnat:projectData/ID' AND read_element=1   JOIN xnat_projectdata proj ON xfm.field_value=proj.id   WHERE u.login='guest'  ) study ON rel.project1=study.id  LEFT JOIN xnat_demographicdata demo ON sub.demographics_xnat_abstractdemographicdata_id=demo.xnat_abstractdemographicdata_id  LEFT JOIN (    SELECT DISTINCT ON (sad.subject_id) sad.subject_id, risks.has_familial_gene, risks.has_other_risk_gene, risks.has_other_risk,apoe         FROM dpuk_dpukriskfactorassessmentdata risks      LEFT JOIN xnat_subjectassessordata sad ON risks.id=sad.id  ) risks ON sub.id=risks.subject_id  LEFT JOIN (        SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_ad, diagnosis_pd, diagnosis_vad, diagnosis_mci, diagnosis_ftd, diagnosis_lbd, mmse, moca,cdr  FROM dpuk_dpukassessmentdata dpuk         LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id LEFT JOIN xnat_experimentData expt ON sad.id=expt.id     ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC  ) dpuk ON sub.id=dpuk.subject_id  LEFT JOIN (         SELECT sad.subject_id, count (expt.id) AS diagnosis_followups, MAX(diagnosis_ad) AS followup_ad, MAX(diagnosis_pd) AS followup_pd, MAX(diagnosis_vad) AS followup_vad, MAX(diagnosis_mci) AS followup_mci, MAX(diagnosis_ftd) AS followup_ftd, MAX(diagnosis_lbd) AS followup_lbd, MAX(mmse) AS followup_mmse, MAX(moca) as followup_moca, MAX(cdr) AS folloup_cdr      FROM dpuk_dpukassessmentdata dpuk         LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id LEFT JOIN xnat_experimentData expt ON sad.id=expt.id     LEFT JOIN (             SELECT DISTINCT ON (sad.subject_id) dpuk.id as BASELINE_ID                FROM dpuk_dpukassessmentdata dpuk                 LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id          LEFT JOIN xnat_experimentData expt ON sad.id=expt.id     ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC       ) baseline ON dpuk.id=baseline.baseline_id      WHERE baseline.baseline_id IS NULL        GROUP BY sad.subject_id  ) FOLLOWUP_DPUK ON sub.id=FOLLOWUP_DPUK.subject_id   LEFT JOIN (         SELECT sad.subject_id, count (expt.id) AS total_diagnosis, MAX(diagnosis_ad) AS max_ad, MAX(diagnosis_pd) AS max_pd, MAX(diagnosis_vad) AS max_vad, MAX(diagnosis_mci) AS max_mci, MAX(diagnosis_ftd) AS max_ftd, MAX(diagnosis_lbd) AS max_lbd, MAX(mmse) AS max_mmse, MAX(moca) as max_moca, MAX(cdr) AS max_cdr      FROM dpuk_dpukassessmentdata dpuk       LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id      GROUP BY sad.subject_id  ) ALL_DPUK ON sub.id=ALL_DPUK.subject_id  LEFT JOIN (    SELECT id, MIN(age_span) as AGE FROM (          SELECT sub.id, CASE (FLOOR(COALESCE(demo.age, sad.age, EXTRACT(YEAR FROM AGE(expt.date, demo.dob)),(EXTRACT(YEAR FROM expt.date)) - (demo.yob)) /10)) WHEN 0 THEN '00-19' WHEN 1 THEN '00-19' WHEN 2 THEN '20-49' WHEN 3 THEN '20-49' WHEN 4 THEN '20-49' WHEN 5 THEN '50-59' WHEN 6 THEN '60-69' WHEN 7 THEN '70-79' WHEN 8 THEN '80+' WHEN 9 THEN '80+' WHEN 10 THEN '80+' WHEN 10 THEN '80+'  ELSE 'Unknown'  END AS AGE_SPAN          FROM xnat_subjectData sub                 LEFT JOIN xnat_demographicdata demo ON sub.demographics_xnat_abstractdemographicdata_id=demo.xnat_abstractdemographicdata_id       LEFT JOIN xnat_subjectAssessorData sad ON sub.id=sad.subject_id          LEFT JOIN xnat_experimentdata expt ON sad.id=expt.id              ORDER BY sub.id,COALESCE(demo.age, sad.age, EXTRACT(YEAR FROM AGE(expt.date, demo.dob)),(EXTRACT(YEAR FROM expt.date)) - (demo.yob))        ) AGE GROUP BY id  ) AGE ON sub.id=AGE.id  LEFT JOIN (    SELECT subject_id, COUNT(pet.id) AS FDG         FROM xnat_petSessionData pet      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id         WHERE pet.tracer_name='FDG'       GROUP BY subject_id  ) FDG ON sub.id=FDG.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(pet.id) AS C11         FROM xnat_petSessionData pet      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id         WHERE pet.tracer_name='C11'       GROUP BY subject_id  ) C11 ON sub.id=C11.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(pet.id) AS F18         FROM xnat_petSessionData pet      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id         WHERE pet.tracer_name='F18'       GROUP BY subject_id  ) F18 ON sub.id=F18.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(pet.id) AS TAU         FROM xnat_petSessionData pet      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id         WHERE pet.tracer_name='TAU'       GROUP BY subject_id  ) TAU ON sub.id=TAU.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(pet.id) AS OTHER_PET   FROM xnat_petSessionData pet      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id         WHERE pet.tracer_name NOT IN ('FDG','C11','F18','TAU') OR pet.tracer_name IS NULL         GROUP BY subject_id  ) OTHER_PET ON sub.id=OTHER_PET.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(pet.id) AS TASK_MEG      FROM xnat_megSessionData pet    LEFT JOIN xnat_imageSessionData isd ON pet.id=isd.id      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id   WHERE isd.session_type='TASK'   GROUP BY subject_id  ) TASK_MEG ON sub.id=TASK_MEG.subject_id  LEFT JOIN (        SELECT subject_id, COUNT(pet.id) AS REST_MEG      FROM xnat_megSessionData pet    LEFT JOIN xnat_imageSessionData isd ON pet.id=isd.id      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id   WHERE isd.session_type='REST'   GROUP BY subject_id  )REST_MEG ON sub.id=REST_MEG.subject_id  LEFT JOIN (         SELECT subject_id, COUNT(pet.id) AS OTHER_MEG     FROM xnat_megSessionData pet    LEFT JOIN xnat_imageSessionData isd ON pet.id=isd.id      LEFT JOIN xnat_subjectAssessorData sad ON pet.id=sad.id  WHERE isd.session_type NOT IN ('TASK','REST') OR isd.session_type IS NULL        GROUP BY subject_id  ) OTHER_MEG ON sub.id=OTHER_MEG.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(id) AS T1_3D FROM (              SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='3D_T1'   ) scans         GROUP BY subject_id  ) T1_3D ON sub.id=T1_3D.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(id) AS T2_3D FROM (            SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='3D_T2'   ) scans         GROUP BY subject_id  ) T2_3D ON sub.id=T2_3D.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(id) AS FLAIR_3D FROM (  SELECT DISTINCT sad.subject_id, sad.id           FROM xnat_imageScanData scan     LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id           WHERE scan.type='3D_FLAIR'        ) scans         GROUP BY subject_id  ) FLAIR_3D ON sub.id=FLAIR_3D.subject_id  LEFT JOIN (        SELECT subject_id, COUNT(id) AS T1_2D FROM (              SELECT DISTINCT sad.subject_id, sad.id          FROM xnat_imageScanData scan              LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='2D_T1'         ) scans         GROUP BY subject_id  ) T1_2D ON sub.id=T1_2D.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(id) AS T2_2D FROM (              SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='2D_T2'  ) scans          GROUP BY subject_id  ) T2_2D ON sub.id=T2_2D.subject_id  LEFT JOIN (      SELECT subject_id, COUNT(id) AS FLAIR_2D FROM (                 SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='2D_FLAIR'        ) scans         GROUP BY subject_id  ) FLAIR_2D ON sub.id=FLAIR_2D.subject_id  LEFT JOIN (        SELECT subject_id, COUNT(id) AS T2_STAR FROM (            SELECT DISTINCT sad.subject_id, sad.id          FROM xnat_imageScanData scan              LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='T2_STAR'       ) scans         GROUP BY subject_id  ) T2_STAR ON sub.id=T2_STAR.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(id) AS SWI FROM (                SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='SWI'   ) scans   GROUP BY subject_id  ) SWI ON sub.id=SWI.subject_id  LEFT JOIN (        SELECT subject_id, COUNT(id) AS REST_MR FROM (            SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='REST'    ) scans         GROUP BY subject_id  ) REST_MR ON sub.id=REST_MR.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(id) AS TASK_MR FROM (          SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan            LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id            WHERE scan.type='TASK'    ) scans         GROUP BY subject_id  ) TASK_MR ON sub.id=TASK_MR.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(id) AS DTI FROM (       SELECT DISTINCT sad.subject_id, sad.id           FROM xnat_imageScanData scan     LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id           WHERE scan.type='DTI'     ) scans         GROUP BY subject_id  ) DTI ON sub.id=DTI.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(id) AS ASL FROM (              SELECT DISTINCT sad.subject_id, sad.id            FROM xnat_imageScanData scan     LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id           WHERE scan.type='ASL'     ) scans         GROUP BY subject_id  ) ASL ON sub.id=ASL.subject_id  LEFT JOIN (  SELECT subject_id, COUNT(id) AS OTHER_MR FROM (          SELECT DISTINCT sad.subject_id, sad.id           FROM xnat_imageScanData scan     LEFT JOIN xnat_subjectAssessorData sad ON scan.image_session_id=sad.id           WHERE scan.type IS NULL OR scan.type NOT IN ('ASL','DTI','TASK','REST','SWI','T2_STAR','2D_FLAIR','2D_T2','3D_FLAIR','3D_T2','3D_T1')       ) scans         GROUP BY subject_id  ) OTHER_MR ON sub.id=OTHER_MR.subject_id";

	private static Object SYNC=new Object();
	
	private static final Map<String,CriteriaField> criteriaMap=new HashMap<String,CriteriaField>();
	    
    private static void addCriteriaMapEntry(String key,String value,FIELD_TYPE type){
    	criteriaMap.put(key.toLowerCase(), new CriteriaField(value,type));
    }
    
    static {
    	addCriteriaMapEntry("cohort", "study",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("freeze", "project",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("age", "age",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("gender", "gender",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("handedness", "handedness",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("education", "education",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("has_familial_gene", "has_familial_gene",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("has_other_risk_gene", "has_other_risk_gene",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("has_other_risk", "has_other_risk",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("apoe", "apoe",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("risk_gene", "apoe",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("diagnosis_ad", "diagnosis_ad",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_pd", "diagnosis_pd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_vad", "diagnosis_vad",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_mci", "diagnosis_mci",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_ftd", "diagnosis_ftd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_lbd", "diagnosis_lbd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("diagnosis_normal", "diagnosis_normal",FIELD_TYPE.BOOLEAN);
    	addCriteriaMapEntry("conversion", "conversion",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("mmse", "mmse",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("moca", "moca",FIELD_TYPE.STRING);
    	addCriteriaMapEntry("cdr", "cdr",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("fdg", "fdg",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("c11", "c11",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("f18", "f18",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("tau", "tau",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("other_pet", "other_pet",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("task_MEG", "task_MEG",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("rest_MEG", "rest_MEG",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("other_meg", "other_meg",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("T1_3D", "T1_3D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("T2_3D", "T2_3D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("FLAIR_3D", "FLAIR_3D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("T1_2D", "T1_2D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("T2_2D", "T2_2D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("FLAIR_2D", "FLAIR_2D",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("T2_STAR", "T2_STAR",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("SWI", "SWI",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("REST_MR", "REST_MR",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("TASK_MR", "TASK_MR",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("DTI", "DTI",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("ASL", "ASL",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("OTHER_MR", "OTHER_MR",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("LONGITUDNAL_IMG", "LONGITUDNAL_IMG",FIELD_TYPE.BOOLEAN);
    	addCriteriaMapEntry("followup_ad", "followup_ad",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_pd", "followup_pd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_vad", "followup_vad",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_mci", "followup_mci",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_ftd", "followup_ftd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_lbd", "followup_lbd",FIELD_TYPE.NUMBER);
    	addCriteriaMapEntry("followup_normal", "followup_normal",FIELD_TYPE.BOOLEAN);
    	addCriteriaMapEntry("diagnosis_followups", "diagnosis_followups",FIELD_TYPE.BOOLEAN);
    	addCriteriaMapEntry("LONG_CLIN", "LONG_CLIN",FIELD_TYPE.BOOLEAN);
    }
	
	public BasicDPUKSummaryStatsImpl() {		
		if(!INITD)
		{
			synchronized (SYNC){
				//this is coded such that the view is dropped and recreated any time tomcat is restarted.  Probably not necessary.  
				//to improve efficiency it would be better to create this as an actual table and recreate it every time a study is published or active_freeze configured.
    			//these queries have no user input, so prepared statements aren't technically required
    			try {
					PoolDBUtils.ExecuteNonSelectQuery("DROP VIEW dpuk_subject_search_view;", null, null);
				} catch (SQLException e) {
					_log.error("",e);
				} catch (Exception e) {
					_log.error("",e);
				}
				try{
					PoolDBUtils.ExecuteNonSelectQuery("CREATE VIEW dpuk_subject_search_view AS " + BASE_QUERY, null, null);
				} catch (SQLException e) {
					_log.error("",e);
				} catch (Exception e) {
					_log.error("",e);
				}
    			INITD=Boolean.TRUE;
			}
		}
	}
    
    public  Map<String,CriteriaField> getCriteriaMap()
    {
    	return criteriaMap;
    }
	
	public void setCriteria(String criteriaString) throws Exception{
    	if(StringUtils.isNotBlank(criteriaString)){
    		final JSONObject jsonCriteria=new JSONObject(criteriaString);
    		
    		criteria=parseCriteria(jsonCriteria.getJSONObject("filters"));
    	}  	
	}
	
	private static final List<String> allowedComparisons=Lists.newArrayList("=",">",">=","<","<=");
    
    private CriteriaI parseCriteria(JSONObject root) throws Exception{
    	if("subset".equals(root.optString("type", "subset"))){
    		final String join=root.optString("join","AND").toUpperCase();
    		if("AND".equals(join) || "OR".equals(join)){
				JSONArray crits=root.getJSONArray("criteria");
				if(crits==null){
					return null;
				}
	
				final CriteriaSet set= new CriteriaSet(join);
				for(int i=0; i<crits.length();i++){
					final JSONObject child=crits.getJSONObject(i);
					CriteriaI childP=parseCriteria(child);
					if(childP!=null){
						set.addCriteria(childP);
					}
				}
				
				if(set.getCriteria().size()==0){
					throw new Exception("No valid criteria defined.");
					
				}else{
					return set;
				}
				
    		}else{
    			throw new Exception("Invalid JOIN type. Must be (AND,OR)");
    		}
    	}else{
    		//add a criteria entry
    		final String field=root.getString("field").toLowerCase();
    		final String comparison=root.optString("comparison","=");
    		final String value=root.getString("value");
    		
    		if(!allowedComparisons.contains(comparison)){
    			throw new Exception("Invalid comparison type. Must be (=,>,>=,<,<=)");
    		}
    		
    		if(!criteriaMap.keySet().contains(field)){
    			throw new Exception("Invalid criteria field.");
    		}
    		
    		if(value.contains("'") || PoolDBUtils.HackCheck(value)){
    			throw new Exception("Invalid criteria value.");
    		}
    		
    		return new Criteria(field,comparison,value);
    	}
    }
	
	private String convertCriteria(CriteriaI crit) throws Exception{
		String criteriaString="(";
		
		if(crit instanceof Criteria){
			if(FIELD_TYPE.STRING.equals(criteriaMap.get(((Criteria) crit).getField()).TYPE)){
				criteriaString+=criteriaMap.get(((Criteria) crit).getField()).KEY + " " + ((Criteria) crit).getComparison()+ " '"+((Criteria) crit).getValue()+"'";

				if(StringUtils.equals(((Criteria)crit).getValue().toString(),"Unknown")){
	    			criteriaString+= " OR " +criteriaMap.get(((Criteria) crit).getField()).KEY + " IS NULL ";
    			}
			}else{
				if(StringUtils.equals(((Criteria)crit).getValue().toString(),"Unknown")){
	    			criteriaString+=criteriaMap.get(((Criteria) crit).getField()).KEY + " IS NULL ";
    			}else{
        			criteriaString+=criteriaMap.get(((Criteria) crit).getField()).KEY + " " + ((Criteria) crit).getComparison()+ " " +((Criteria) crit).getValue();
    			}
			}
			
		}else{
			final String join=((CriteriaSet)crit).getJoin();
			int critCount=0;
			for(CriteriaI child : ((CriteriaSet)crit).getCriteria()){
				if(critCount++>0){
					criteriaString +=" "+join+" ";
				}
				criteriaString+=convertCriteria(child);
			}
		}
		
		criteriaString +=")";
		return criteriaString;
	}
	
	private String buildSQL() throws Exception{
		final String query= "SELECT * FROM dpuk_subject_search_view ";
		if(criteria==null){
			return query;
		}else{
			return query + " WHERE " + convertCriteria(criteria);
		}
		
	}
	
	private JSONArray addDefaultValue(JSONArray values, String key, Object def){
		boolean matched=false;
		for(int i=0; i<values.length();i++){
			if(values.getJSONObject(i).getString("key").equals(key)){
				matched=true;
				break;
			}
		}
		if(!matched){
			final JSONObject obj= new JSONObject();
			obj.put("key", key);
			obj.put("value", def);
			values.put(obj);
		}
		return values;
	}
	
	@Override
	public JSONArray getStat(String key) throws Exception{
		String query="";
		
		switch (key){
			case "cohorts":
				query = "SELECT study AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY study ORDER BY study;";
				return defaultCountHandler(query);
			case "age":
				query = "SELECT age AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY age ORDER BY age;";
				return defaultCountHandler(query);
			case "gender":
				query = "SELECT gender AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY gender ORDER BY gender;";
				return addDefaultValue(addDefaultValue(addDefaultValue(defaultCountHandler(query),"Male",0),"Female",0),"Unknown",0);
			case "education":
				query = "SELECT education AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY education ORDER BY education;";
				return defaultCountHandler(query);
			case "handedness":
				query = "SELECT handedness AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY handedness ORDER BY handedness;";
				return defaultCountHandler(query);
			case "risk_gene":
				query = "SELECT apoe AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY apoe ORDER BY apoe;";
				return defaultCountHandler(query);
			case "familial":
				query = "SELECT has_familial_gene AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY has_familial_gene ORDER BY has_familial_gene;";
				return addDefaultValue(addDefaultValue(addDefaultValue(defaultCountHandler(query),"Positive",0),"Negative",0),"Unknown",0);
			case "other_risk":
				query = "SELECT has_other_risk AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY has_other_risk ORDER BY has_other_risk;";
				return addDefaultValue(addDefaultValue(addDefaultValue(defaultCountHandler(query),"Positive",0),"Negative",0),"Unknown",0);
			case "mr_types":
				query = "SELECT type, COUNT(id) FROM (SELECT sub.id, CASE WHEN scan.type IN ('ASL','DTI','TASK','REST','SWI','T2_STAR','2D_FLAIR','2D_T2','3D_FLAIR','3D_T2','3D_T1') THEN scan.type ELSE 'Other' END AS type, COUNT(scan.xnat_imagescandata_id) AS TYPE_COUNT FROM ("+buildSQL() + ") sub JOIN xnat_subjectAssessorData sad ON sub.id=sad.subject_id JOIN xnat_imageScanData scan ON sad.id=scan.image_session_id GROUP BY sub.id, CASE WHEN scan.type IN ('ASL','DTI','TASK','REST','SWI','T2_STAR','2D_FLAIR','2D_T2','3D_FLAIR','3D_T2','3D_T1') THEN scan.type ELSE 'Other' END ORDER BY sub.id, CASE WHEN scan.type IN ('ASL','DTI','TASK','REST','SWI','T2_STAR','2D_FLAIR','2D_T2','3D_FLAIR','3D_T2','3D_T1') THEN scan.type ELSE 'Other' END)SRCH GROUP BY type ORDER BY type";
				return defaultCountHandler(query);
			case "pet_types":
				query = "SELECT tracer_name, COUNT(id) FROM (SELECT sub.id, CASE WHEN scan.tracer_name IN ('FDG','C11','F18','TAU') THEN scan.tracer_name ELSE 'Other' END AS tracer_name, COUNT(scan.ID) AS TYPE_COUNT FROM ("+buildSQL() + ") sub JOIN xnat_subjectAssessorData sad ON sub.id=sad.subject_id JOIN xnat_petSessionData scan ON sad.id=scan.id GROUP BY sub.id, CASE WHEN scan.tracer_name IN ('FDG','C11','F18','TAU') THEN scan.tracer_name ELSE 'Other' END ORDER BY sub.id, CASE WHEN scan.tracer_name IN ('FDG','C11','F18','TAU') THEN scan.tracer_name ELSE 'Other' END)SRCH GROUP BY tracer_name ORDER BY tracer_name";
				return defaultCountHandler(query);
			case "meg_types":
				query = "SELECT session_type, COUNT(id) FROM (SELECT sub.id, CASE WHEN isd.session_type IN ('TASK','REST') THEN isd.session_type ELSE 'Other' END AS session_type, COUNT(meg.ID) AS TYPE_COUNT FROM ("+buildSQL() + ") sub JOIN xnat_subjectAssessorData sad ON sub.id=sad.subject_id JOIN xnat_megSessionData meg ON sad.id=meg.id JOIN xnat_imageSessiondata isd ON meg.id=isd.id GROUP BY sub.id, CASE WHEN isd.session_type IN ('TASK','REST') THEN isd.session_type ELSE 'Other' END ORDER BY sub.id, CASE WHEN isd.session_type IN ('TASK','REST') THEN isd.session_type ELSE 'Other' END)SRCH GROUP BY session_type ORDER BY session_type";
				return defaultCountHandler(query);
			case "diagnosis":
				query = "SELECT DIAGNOSISS.diagnosis, COUNT(DIAGNOSISS.value) FROM ("+buildSQL() + ") SUB LEFT JOIN (  SELECT subject_id, 'Diagnosis_AD' AS diagnosis, diagnosis_ad AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_ad	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_ad>0	 UNION	 SELECT subject_id, 'Diagnosis_PD' AS diagnosis, diagnosis_pd AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_pd	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_pd>0	 UNION	 SELECT subject_id, 'Diagnosis_VAD' AS diagnosis, diagnosis_vad AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_vad	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_vad>0	 UNION	 SELECT subject_id, 'Diagnosis_MCI' AS diagnosis, diagnosis_mci AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_mci	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_mci>0	 UNION	 SELECT subject_id, 'Diagnosis_FTD' AS diagnosis, diagnosis_ftd AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_ftd	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_ftd>0	 UNION	 SELECT subject_id, 'Diagnosis_LBD' AS diagnosis, diagnosis_lbd AS VALUE FROM (  SELECT DISTINCT ON (sad.subject_id) sad.subject_id, diagnosis_lbd	  FROM dpuk_dpukassessmentdata dpuk	  LEFT JOIN xnat_subjectassessordata sad ON dpuk.id=sad.id	  LEFT JOIN xnat_experimentData expt ON sad.id=expt.id	  ORDER BY sad.subject_id, CASE WHEN expt.delay IS NULL THEN 0 ELSE expt.delay END ASC ) srch WHERE diagnosis_lbd>0	) DIAGNOSISS ON sub.id=DIAGNOSISS.subject_id GROUP BY DIAGNOSISS.diagnosis";
				return defaultCountHandler(query);
			case "conversion":
				query = "SELECT conversion AS key, COUNT (id) AS COUNT from ("+buildSQL() + ") BASE GROUP BY conversion ORDER BY conversion;";
				return defaultCountHandler(query);
			case "mmse":
				query = "SELECT sub.mmse, COUNT(sub.ID) AS COUNT FROM ("+buildSQL() + ") sub GROUP BY mmse ORDER BY mmse;";
				return defaultCountHandler(query);
			case "moca":
				query = "SELECT sub.moca, COUNT(sub.ID) AS COUNT FROM ("+buildSQL() + ") sub GROUP BY moca ORDER BY moca;";
				return defaultCountHandler(query);
			case "cdr":
				query = "SELECT sub.cdr, COUNT(sub.ID) AS COUNT FROM ("+buildSQL() + ") sub GROUP BY cdr ORDER BY cdr;";
				return defaultCountHandler(query);
			case "long_clin":
				query = "SELECT sub.LONG_CLIN, COUNT(sub.ID) AS COUNT FROM ("+buildSQL() + ") sub GROUP BY LONG_CLIN ORDER BY LONG_CLIN;";
				return addDefaultValue(addDefaultValue(defaultCountHandler(query),"True",0),"False",0);
			case "long_image":
				query = "SELECT sub.LONGITUDNAL_IMG, COUNT(sub.ID) AS COUNT FROM ("+buildSQL() + ") sub GROUP BY LONGITUDNAL_IMG ORDER BY LONGITUDNAL_IMG;";
				return addDefaultValue(addDefaultValue(defaultCountHandler(query),"True",0),"False",0);
		}
		return defaultCountHandler(query);
	}
	
	@SuppressWarnings("unchecked")
	private JSONArray defaultCountHandler(String query){
		final JSONArray values=new JSONArray();
		
		if(query!=""){
			try {
				final XFTTable t=XFTTable.Execute(query, null, null);
				@SuppressWarnings("rawtypes")
				final List<List> dbValues=t.toArrayListOfLists();
				for(List<Object> row: dbValues){
					try {
						final JSONObject obj= new JSONObject();
						if(row.get(0)==null){
							obj.put("key", "Unknown");
						}else{
							obj.put("key", row.get(0).toString());
						}
						obj.put("value", row.get(1));
						values.put(obj);
					} catch (JSONException e) {
						_log.error("",e);
					}
				}
			} catch (SQLException e) {
				_log.error("",e);
			} catch (DBPoolException e) {
				_log.error("",e);
			}
		}
		return values;
	}

	@Override
	public Number getSubjectCount() {
		try {
			final String query = "SELECT COUNT (id) AS COUNT from ("+buildSQL() + ") BASE;";

			return (Number)PoolDBUtils.ReturnStatisticQuery(query,"count", null, null);
		} catch (SQLException e) {
			_log.error("",e);
		} catch (Exception e) {
			_log.error("",e);
		}
		return 0;
	}
	
	@Override
	public List<Object[]> getSubjectIDs() throws Exception{
		final String query = "SELECT DISTINCT Study_pk, project, id as subject, study, project_display FROM ("+buildSQL() + ") BASE;";
		final XFTTable t=XFTTable.Execute(query, null, null);
		return t.rows();
	}
	   
    
    public interface CriteriaI{}
        
    public class Criteria implements CriteriaI{
    	public Criteria(String field, String comparison, Object value) {
    		this.field=field;
    		this.comparison=comparison;
    		this.value=value;    		
    	}
    	public String getField() {
			return field;
		}
		public String getComparison() {
			return comparison;
		}
		public Object getValue() {
			return value;
		}
		private final String field;
    	private final String comparison;
    	private final Object value;
    }

    public class CriteriaSet implements CriteriaI{
    	public CriteriaSet(String join){
    		this.join=join;
    	}
    	public String getJoin() {
			return join;
		}
		public List<CriteriaI> getCriteria() {
			return criteria;
		}
		public void addCriteria(CriteriaI crit){
			this.criteria.add(crit);
		}
		private final String join;
    	private List<CriteriaI> criteria=Lists.newArrayList();
    }
    
	private static Boolean INITD=Boolean.FALSE;

	private CriteriaI criteria=null;
	
}
