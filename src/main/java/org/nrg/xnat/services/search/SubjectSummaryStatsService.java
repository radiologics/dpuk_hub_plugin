package org.nrg.xnat.services.search;

import java.util.Map;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to support real time querying of pre-defined subject fields as summary scores.
 * 
 * This service is used to instantiate a SubjectSummaryStatsI implementation
 *
 */
public class SubjectSummaryStatsService {
	
	public enum FIELD_TYPE{STRING,BOOLEAN,NUMBER};

    /**
     * Object used in the identification of fields that can be used to build criteria
     */
    public static class CriteriaField{
    	public final String KEY;
    	public final FIELD_TYPE TYPE;
    	
    	public CriteriaField(String key, FIELD_TYPE type){
    		KEY=key;
    		TYPE=type;
    	}
    }
   
    
	/**
	 * Method to create a Subject Summary stats object based on 
	 * @param criteriaString
	 * @return
	 * @throws Exception
	 */
	public static SubjectSummaryStatsI create(String criteriaString) throws Exception{
		final SubjectSummaryStatsI stats=SubjectSummaryStatsService.create();
    	stats.setCriteria(criteriaString);
    	return stats;
	}	
	
	/**
	 * 
	 * @return
	 */
	public static Map<String,CriteriaField> getCriteriaFields(){
		final SubjectSummaryStatsI stats=SubjectSummaryStatsService.create();
		return stats.getCriteriaMap();
	}
	
	/**
	 * Create the actual Subject Summary instance
	 * @return
	 */
	private static SubjectSummaryStatsI create(){
		return new BasicDPUKSummaryStatsImpl();
	}
}
