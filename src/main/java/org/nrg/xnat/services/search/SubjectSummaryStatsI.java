package org.nrg.xnat.services.search;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.nrg.xnat.services.search.SubjectSummaryStatsService.CriteriaField;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to support real time querying of pre-defined subject fields as summary scores.
 * 
 * This interface is the access point for retrieve statistics about stored data.
 *
 */
public interface SubjectSummaryStatsI {
	
	/**
	 * Retrieves the number of subjects who match the query
	 * @return
	 */
	public Number getSubjectCount();
	
	/**
	 * Returns an individual statistic for a specified key.
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public JSONArray getStat(String key) throws Exception;
	
	/**
	 * Returns the criteria fields which can be used with this Subject Summary object.
	 * @return
	 */
	public Map<String,CriteriaField> getCriteriaMap(); 
	
	/**
	 * Sets the criteria to be applied when retrieving data fromt his Subject Summary object.
	 * @param criteriaString
	 * @throws Exception
	 */
	public void setCriteria(String criteriaString) throws Exception;
	
	/**
	 * Retrieves the matching subject IDs for this Subject Summary object.
	 * @return List<List> : Inner list has 3 columns study,project,subject
	 * @throws Exception 
	 */
	public List<Object[]> getSubjectIDs() throws Exception;
}
