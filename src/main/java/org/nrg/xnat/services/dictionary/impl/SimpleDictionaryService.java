package org.nrg.xnat.services.dictionary.impl;

import java.util.HashMap;
import java.util.Map;

import org.nrg.xnat.services.dictionary.DictionaryService;
import org.springframework.stereotype.Service;

@Service
public class SimpleDictionaryService implements DictionaryService {

	final String THEDEFAULT="default";
	final String THEDELIMITER=":";
	final String XNATNAMESPACE="xnat";
	final String XDATNAMESPACE="xdat";

	
	@Override
	public Map<String,String> mapAll(Map<String,String> params,String type){
		Map<String,String> newmap = new HashMap<String, String>();
		
		for (String key : params.keySet()) {
			newmap.put(this.map(key, type), params.get(key));
		}
		return newmap;
		
	}
	@Override
	//xnat:stuff/name
	//xnat:stuff.name
	//stuff
	
	
	public String map(String key,String type) {
		String[] pieces = key.split(THEDELIMITER);
		if(pieces.length==2 && !key.startsWith(type) && !key.startsWith(XNATNAMESPACE)&&!key.startsWith(XDATNAMESPACE)){
			if(key.startsWith(THEDEFAULT)){
				return type+"/"+pieces[1];
			}else{
				return type+"/fields/field[name="+key+"]/field";
			}
		}
		return key;
	}

}
