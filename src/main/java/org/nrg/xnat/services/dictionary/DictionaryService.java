package org.nrg.xnat.services.dictionary;

import java.util.Map;

public interface DictionaryService {
	Map<String, String> mapAll(Map<String, String>  map,String type);
	String map(String key,String type);
}
