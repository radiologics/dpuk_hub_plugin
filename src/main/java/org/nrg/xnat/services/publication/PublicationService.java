package org.nrg.xnat.services.publication;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.entities.Publication;

import java.io.IOException;
import java.util.List;

/**
 * Created by mmckay01 on 1/29/14.
 */
public interface PublicationService extends BaseHibernateService<Publication>{
    public static String SERVICE_NAME = "PublicationService";

    public List<Publication> getPublicationsForProject(String project);

    public List<Publication> getPublicationsByIdAndProject(String id,String project);

    public String toJson(final List<Publication> publications) throws IOException;
}
