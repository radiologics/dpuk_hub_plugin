package org.nrg.xnat.entities;

/**
 * Created by mmckay01 on 1/29/14.
 */

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"project","pmid"}))
public class Publication extends AbstractHibernateEntity{
	private static final long serialVersionUID = 2450425633920858320L;
	private String articleTitle;
    private String authors;
    private String publication;
    private String date;
    private String pmid;
    private String project;
    private String doi;

    public Publication() {
    }

    public Publication(String articleTitle, String authors, String publication, String date, String pmid, String project, String doi) {
        this.articleTitle = articleTitle;
        this.authors = authors;
        this.publication = publication;
        this.date = date;
        this.pmid = pmid;
        this.project = project;
        this.doi = doi;
    }

    public String getArticleTitle(){
        return articleTitle;
    }

    public String getAuthors(){
        return authors;
    }

    public String getPublication(){
        return publication;
    }

    public String getDate(){
        return date;
    }

    public String getPmid(){
        return pmid;
    }

    public String getProject(){
        return project;
    }

    public String getDoi(){
        return doi;
    }

    public void setArticleTitle(String articleTitle){
        this.articleTitle = articleTitle;
    }

    public void setAuthors(String authors){
        this.authors = authors;
    }

    public void setPublication(String publication){
        this.publication = publication;
    }

    public void setDate(String date){
        this.date = date;
    }

    public void setPmid(String pmid){
        this.pmid = pmid;
    }

    public void setProject(String project){
        this.project = project;
    }

    public void setDoi(String doi){
        this.doi = doi;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof Publication)) {
            return false;
        }
        Publication other = (Publication) object;
        return           (StringUtils.equals(getArticleTitle(),other.getArticleTitle()) &&
                StringUtils.equals(getPublication(),other.getPublication()) &&
                StringUtils.equals(getDate(),other.getDate()) &&
                StringUtils.equals(getPmid(),other.getPmid()) &&
                StringUtils.equals(getProject(),other.getProject()) &&
                StringUtils.equals(getDoi(),other.getDoi()) &&
                StringUtils.equals(getAuthors(),other.getAuthors()));
    }


}
