package org.nrg.xnat.restlet.util;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nrg.framework.utilities.Reflection;
import org.nrg.xnat.restlet.util.SecureResourceParameterMapper;
import org.nrg.xnat.services.dictionary.DictionaryService;
import org.springframework.stereotype.Component;

@Component("dictionaryServiceParameterMapper")
public class DictionaryServiceParameterMapper implements SecureResourceParameterMapper{
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DictionaryServiceParameterMapper.class);
    
    private List<DictionaryService> getDictionaries() {
        List<DictionaryService> dictionaries=new ArrayList<DictionaryService>();
        
            List<Class<?>> classes;
            try {
                classes = Reflection.getClassesForPackage("org.nrg.xnat.services.dictionary.impl");
            
                for (Class<?> clazz : classes) {
                    dictionaries.add((DictionaryService)clazz.newInstance());
                }
            } catch (Exception exception) {
                logger.warn("No dictionaries found");
            }
        
        return dictionaries;
    }
    
    @Override
    public Map<String, String> mapParams(Map<String,String> params, String dataType){
     List<DictionaryService> dicts=getDictionaries();
         for (DictionaryService dict : dicts) {
             params = dict.mapAll(params, dataType);
         }
         return params;
    }
}
