package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.services.CARManagementService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatExperimentdataShare;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatProjectparticipant;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatExperimentdata;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xdat.om.base.BaseXnatSubjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.EventUtils.CATEGORY;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import com.google.common.collect.Lists;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to share subject data for projects associated with a Cohort Access Request (CAR)
 * 
 */
@XnatRestlet({"/services/cars/{ID}/share"})
public class CARsShareRestlet extends SecureResource {

	private static final Log _log = LogFactory.getLog(CARsShareRestlet.class);
	
	private Integer carId=null;

	public CARsShareRestlet(Context context, Request request, Response response) {
        super(context, request, response);
        String carS=(String)getParameter(request,"ID");
		if(NumberUtils.isNumber(carS)){
			carId= Integer.parseInt(carS);
		}else{
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return;
		}
        this.getVariants().add(new Variant(MediaType.ALL));
	}
    
    @Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {
        try {
        	//retrieve pertinent CAR entry
        	CARManagementService _service =XDAT.getContextService().getBean(CARManagementService.class);
	    	final CohortAccessRequest car = _service.retrieve(carId);
	        if (car == null) {
	            this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
	            return ;
	        }
	        
	        //only share CARs that are approved.
			if(! CohortStatus.Approved.equals(car.getStatus())){
				if(CohortStatus.Complete.equals(car.getStatus())){
					 this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Access Request already completed");
			         return ;
				}else{
					 this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Access Request must be approved");
			         return ;
				}
			}
	        
			//parse the project identification information from the body of the HTML request
			String projectSettingsS=null;
	        if(this.getRequest().isEntityAvailable()){
				Representation entity=this.getRequest().getEntity();
				if(RequestUtil.hasContent(entity)){
					try {
						projectSettingsS=IOUtils.toString(entity.getStream());
					} catch (IOException e) {
			 			_log.error("",e);
			 			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e);
			 			return;
					}
				}
	    	}
	        JSONObject bodyParams=new JSONObject(projectSettingsS);
	        					
			if(("default".equals(bodyParams.optString("secondary_id","default")))){
				this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Requires project secondary_id");
		         return ;
			}

			if(("default".equals(bodyParams.optString("name","default")))){
				this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Requires project name");
		         return ;
			}
			
			XDATUser otherUser=new XDATUser(car.getUsername());

			//beginning actual database modifications...
			car.setStatus(CohortStatus.In_Progress);
			_service.update(car);

			//create a summary tracker to track the status of each step (in case we want to generate a manifest)
            List<String[]> _results=Lists.newArrayList();
            
            //create the project that will store the subject data.
            //if the project already exists, confirm it is a DPUK_SHARE project and the otherUser has access to it
			XnatProjectdata proj=null;
			try {
				List<XnatProjectdata> projects=BaseXnatProjectdata.getXnatProjectdatasByField("xnat:projectData/secondary_ID", bodyParams.optString("secondary_id"), null, false);
				if(projects!=null && projects.size()>0){
					proj=projects.get(0);
				}
				
				if(proj!=null){
					if(!StringUtils.equals("DPUK_SHARE", proj.getStringProperty("xnat:projectData/type"))){
						this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Project already exists but is not a DPUK_SHARE project");
				         return ;
					}
					
					if(proj.canCreate(otherUser)){
						this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Project already exists but this user doesn't have access");
				         return ;
					}
				}else{
					proj = new XnatProjectdata((UserI)otherUser);
					proj.setId(otherUser.getUsername()+"_"+RandomStringUtils.randomAlphanumeric(5));
					proj.setSecondaryId(bodyParams.optString("secondary_id"));
					proj.setName(bodyParams.optString("name"));
					if(!("default".equals(bodyParams.optString("desc","default")))){
						proj.setDescription(bodyParams.optString("desc"));
					}
					proj.setProperty("xnat:projectData/type", "DPUK_SHARE");
					
					proj.preSave();
					BaseXnatProjectdata.createProject(proj, otherUser, true, true, newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN,"DPUK Project init"), "private");
					proj =XnatProjectdata.getProjectByIDorAlias(proj.getId(), null, false);
				}
			} catch (Exception e1) {
				logger.error("",e1);
				
				car.setStatus(CohortStatus.Approved);
				_service.update(car);
				
				_results.add(new String[]{"Create project","Failed"});
	 			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e1);
	 			return;
			}
			
			car.setCreatedProject(proj.getId());
			_service.update(car);
			
			//build a list of the approved projects for subject sharing
			List<Subcohort> subcohorts=car.getCohorts();
			List<String> approvedProjects=Lists.newArrayList();
			for (Subcohort sc: subcohorts){
				if(CohortStatus.Approved.equals(sc.getStatus())){
					approvedProjects.add(sc.getProject());
				}
			}
			
            //iterate list of subjects and share them (and their experiments) into the user's new project.
            for(String[] subjects :car.getResultsAsList()){
 				String study=subjects[0];
 				String project=subjects[1];
 				String subject=subjects[2]; 				
 				
 				if(approvedProjects.contains(project)){
	 				XnatSubjectdata subj=XnatSubjectdata.getXnatSubjectdatasById(subject, null, false);

 					//Make sure the subject isn't already in the project
	 				XnatProjectparticipantI exists=null;
	 				for(XnatProjectparticipantI pp: subj.getSharing_share()){
	 					if(StringUtils.equals(pp.getProject(), proj.getId())){
	 						//subject already shared into this project
	 						exists=pp;
	 					}
	 				}
	 				
	 				if(exists!=null){
	 					_results.add(new String[]{study + ": Shared subject " + subj.getLabel() + " already exists as "+ exists.getLabel(),"Skipped"});
	 					continue;
	 				}
	 				
	 				try {
	 					//share subject into new project
						XnatProjectparticipant pp = new XnatProjectparticipant((UserI) otherUser);
						pp.setProject(proj.getId());
						pp.setLabel(subj.getProject()+"_"+subj.getLabel());
						pp.setSubjectId(subj.getId());
						BaseXnatSubjectdata.SaveSharedProject(pp, subj, otherUser, newEventInstance(CATEGORY.DATA,"DPUK Project share"));
						
						_results.add(new String[]{study + ": Shared subject " + subj.getLabel() + " as "+ pp.getLabel(),"Success"});

	 					//share all this subject's experiments into the new project
						for(XnatSubjectassessordataI expt: subj.getExperiments_experiment()){
			 				try {
								XnatExperimentdataShare exptPP= new XnatExperimentdataShare((UserI)otherUser);
								exptPP.setProject(proj.getId());
								exptPP.setLabel(expt.getProject()+"_"+expt.getLabel());
								exptPP.setProperty("sharing_share_xnat_experimentda_id", expt.getId());
								BaseXnatExperimentdata.SaveSharedProject((XnatExperimentdataShare)exptPP, (XnatExperimentdata)expt, otherUser,newEventInstance(EventUtils.CATEGORY.DATA,"DPUK Project share"));
	
								_results.add(new String[]{study + ": Shared experiment " + expt.getLabel() + " as "+ exptPP.getLabel(),"Success"});
			 				} catch (Exception e) {
								logger.error("",e);
								_results.add(new String[]{study + ": Shared experiment " + expt.getLabel() + " as "+ expt.getProject()+"_"+expt.getLabel(),"Failed"});
							}
						}
					} catch (Exception e) {
						logger.error("",e);
						_results.add(new String[]{study + ": Shared subject " + subj.getLabel() + " as "+ subj.getProject()+"_"+subj.getLabel(),"Failed"});
					}
 				}
 			}

            //update CAR status to complete
			car.setStatus(CohortStatus.Complete);
			_service.update(car);
 			
			
			this.getResponse().setEntity(new StringRepresentation(proj.getId()));
 		} catch (JSONException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e);
 		} catch (Exception e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e);
 		}
	}
}


