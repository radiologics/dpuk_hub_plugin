package org.nrg.xnat.restlet.extensions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.exception.InvalidItemException;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.JSONObjectRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

@XnatRestlet({"/services/cohort/{ID}/releaseSummary"})
public class CurrentUserRestlet extends SecureResource {
	private static final Log _log = LogFactory.getLog(CurrentUserRestlet.class);
	String study;
	private UserI user;
	public CurrentUserRestlet(Context context, Request request, Response response) {
		super(context, request, response);
		study=(String)getParameter(request,"ID");
        this.getVariants().add(new Variant(MediaType.ALL));
        user = this.getUser();
	}
	
    @Override
    public Representation represent(Variant variant) throws ResourceException {
    	XnatProjectdata proj=XnatProjectdata.getProjectByIDorAlias(study, null, false);
		if(proj==null){
			this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return null;
		}
		
		try {
			if(!proj.canRead(user)){
				return null;
			}
		} catch (InvalidItemException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
 			return null;
		} catch (Exception e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
 			return null;
		}
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("project",proj.getId());
		
		String query="SELECT releaseP.id, releaseP.secondary_id, releaseP.name, releaseP.description, count(sub.id) AS subjects FROM xnat_subjectData sub LEFT JOIN xnat_projectData releaseP ON sub.project=releaseP.id LEFT JOIN xhbm_project_relationship release ON sub.project=release.project2  WHERE release.type='freeze' AND release.project1=:project GROUP BY releaseP.id, releaseP.secondary_id, releaseP.name, releaseP.description";
		
		try {
			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			SqlRowSet set = jdbcTemplate.queryForRowSet(query, parameters);
			
			JSONObject response=new JSONObject();
			JSONArray array=new JSONArray();
			response.put("relations", array);
			set.beforeFirst();
			while (set.next()) {
				JSONObject row= new JSONObject();
				row.put("ID",set.getString("id"));
				row.put("secondary_id",set.getString("secondary_id"));
				row.put("name",set.getString("name"));
				row.put("description",set.getString("description"));
				row.put("subjects",set.getInt("subjects"));
				array.put(row);
			}
   
			return new JSONObjectRepresentation(MediaType.APPLICATION_JSON, response);
		} catch (InvalidResultSetAccessException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
 			return null;
		} catch (DataAccessException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
 			return null;
		} catch (JSONException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
 			return null;
		}
    }
}
