package org.nrg.xnat.restlet.extensions;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.services.project.study.StudyException;
import org.nrg.xnat.services.project.study.StudyService;
import org.nrg.xnat.services.project.study.StudySummary;
import org.restlet.Context;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Resource;
import org.restlet.resource.StringRepresentation;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author james@radiologics.com
 *
 */

/*
 * 
GET /data/studysummary
 * 
 */
@XnatRestlet(value = {"/studysummary","/services/studysummary"}, secure = false)
public class StudySummaryRestlet extends Resource {

    
    private static final ObjectMapper MAPPER = new ObjectMapper();

	static Logger logger = Logger.getLogger(StudySummaryRestlet.class);
	final static String PROPERTIES="properties";
	final static String ID="id";
	final static String NAME="name";
	final static String TYPE="type";
	final static String INSERTDATE="insertDate";
	final static String ACCESSIBILITY="accessibility";
	final static String SECONDARY_ID="secondary_id";

	
	
	final static Boolean SEARCHTERMNOTREQURED=Boolean.TRUE;

	

	final StudyService studyService;
	

	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public StudySummaryRestlet(Context context, Request request, Response response) {
		super(context, request, response);
		this.studyService=XDAT.getContextService().getBean(StudyService.class);
	}
	
	@Override
	public boolean allowPost() {
		return false;
	}
	@Override
	public boolean allowDelete() {
		return false;
	}
	@Override
	public boolean allowGet() {
		return true;
	}
	@Override
	public boolean allowPut() {
		return false;
	}
	

	
	
	boolean validate(XDATUser user,String searchTerm) throws Exception{
		//
		if(StringUtils.isBlank(searchTerm)){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing search parameters");
			return false;
		}
		return true;
	}


	@Override
	public void handleGet() {
		
		try {
			
			
			StudySummary summary=this.studyService.getStudySummary();
			if(summary==null){
				throw new StudyException("Summary not found!");
			}
			this.getResponse().setEntity(new StringRepresentation(MAPPER.writeValueAsString(summary)));
			
			this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
			
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}

	
	
	
	
}
