package org.nrg.xnat.restlet.extensions;

/*
 * org.nrg.xnat.restlet.extensions.InvestigatorRestlet
 * XNAT http://www.xnat.org
 * Copyright (c) 2015, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 8/26/2015 5:31 PM
 */

import org.apache.commons.lang3.StringUtils;
import org.nrg.action.ActionException;
import org.nrg.xdat.om.XnatInvestigatordata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.exception.InvalidItemException;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.ItemResource;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.utils.WorkflowUtils;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/*
 * Example PUT request
 * curl -X PUT -u username:password http://localhost:8080/xnat/data/investigator/ --data 'default%3Atitle=Mr&default%3Afirstname=bossszo&default%3AID=EUTYWOUTY&default%3Alastname=Picknose&default%3Ainstitution=myinstalsdfglationhere&default%3Adepartment=dept&default%3Aemail=me%40here.com&default%3Aphone=666666666666'
 * 
 * Example DELETE request
 * curl -X DELETE -u admin:admin http://localhost:8080/xnat/data/investigator/6
 * 
 * 
 */
@XnatRestlet({ "/investigator/{INVESTIGATOR_ID}", "/investigator" })
public class InvestigatorRestlet extends ItemResource {
	private static final Logger logger = LoggerFactory.getLogger(InvestigatorRestlet.class);

	public XnatInvestigatordata investigator = null;
	private String invID = null;
	private UserI user;

	public InvestigatorRestlet(Context context, Request request,
			Response response) throws ResourceException {
		super(context, request, response);
		user = this.getUser();

		

		invID = (String) getParameter(request, "INVESTIGATOR_ID");
		if (invID != null) {
			investigator = XnatInvestigatordata.getXnatInvestigatordatasByXnatInvestigatordataId(invID, user, false);
		}

		if (investigator != null) {
			getVariants().add(new Variant(MediaType.TEXT_HTML));
			getVariants().add(new Variant(MediaType.TEXT_XML));
		}
	}

	@Override
	public boolean allowDelete() {
		return true;
	}

	@Override
	public boolean allowPut() {
		return true;
	}

	@Override
	public void handleDelete() {
		if (user == null || user.isGuest()) {
			getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
		} else {
			if (investigator != null) {
				try {
					if (investigator.canDelete(user)) {
						final PersistentWorkflowI workflow = WorkflowUtils
								.getOrCreateWorkflowData(
										getEventId(),
										user,
										XnatInvestigatordata.SCHEMA_ELEMENT_NAME,
										String.valueOf(investigator
												.getXnatInvestigatordataId()),
												String.valueOf(investigator
														.getXnatInvestigatordataId()),
														newEventInstance(
																EventUtils.CATEGORY.PROJECT_ADMIN,
																EventUtils
																.getDeleteAction(investigator
																		.getXSIType())));
						final EventMetaI ci = workflow.buildEvent();

						try {							
							SaveItemHelper.authorizedDelete(investigator.getItem(), user, ci);
							PersistentWorkflowUtils.complete(workflow, ci);
						} catch (Exception e) {
							logger.error("", e);
							PersistentWorkflowUtils.fail(workflow, ci);
							throw e;
						}
					} else {
						getResponse()
						.setStatus(Status.CLIENT_ERROR_FORBIDDEN,
								"User account doesn't have permission to delete this investigator.");
					}
				} catch (InvalidItemException e) {
					logger.error("", e);
					getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
				} catch (Exception e) {
					logger.error("", e);
					getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
				}
			}
		}
	}
//need firstname and lastname
	@Override
	public void handlePut() {
		if (user == null || user.isGuest()) {
			getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
		} else {
			
			try {
				String fName = SecureResource.getQueryVariable("xnat:investigatorData.firstname", getRequest());
		        String lName = SecureResource.getQueryVariable("xnat:investigatorData.lastname", getRequest());
		        
				if(!StringUtils.isBlank(fName) && !StringUtils.isBlank(lName)){
				
					final PersistentWorkflowI workflow = WorkflowUtils.getOrCreateWorkflowData(getEventId(), user, XnatInvestigatordata.SCHEMA_ELEMENT_NAME, null, null, newEventInstance(EventUtils.CATEGORY.PROJECT_ADMIN, EventUtils.getAddModifyAction(XnatInvestigatordata.SCHEMA_ELEMENT_NAME, true)));
					final EventMetaI ci = workflow.buildEvent();
	
					if (investigator == null || investigator.canEdit(user)) {
						XFTItem item = loadItem("xnat:investigatorData", true);
	
						if (item == null) {
							String xsiType = getQueryVariable("xsiType");
							if (xsiType != null) {
								item = XFTItem.NewItem(xsiType, user);
							}
						}	
	
						if (item == null) {
							if (investigator != null) {
								item = investigator.getItem();
							}
						}
	
						if (item == null) {
							getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED, "Need PUT Contents");
							return;
						}
	
						boolean allowDataDeletion = false;
						if (getQueryVariable("allowDataDeletion") != null && getQueryVariable("allowDataDeletion").equalsIgnoreCase("true")) {
							allowDataDeletion = true;
						}
						if(item.instanceOf("xnat:investigatorData")){ 
							XnatInvestigatordata inv = new XnatInvestigatordata(item);
							SaveItemHelper.authorizedSave(inv, user, false, allowDataDeletion, ci);
						}
						else {
							getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "User account doesn't have permission to edit this investigator.");
						}
					}
				}  else{
					
			            logger.warn("Firstname and Lastname and required");
						this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing Firstname and Lastname");

			      
				}
			} catch (ActionException e) {
				getResponse().setStatus(e.getStatus(),e.getMessage());
			} catch (InvalidPermissionException e) {
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, e.getMessage());
			} catch (IllegalArgumentException e) {
				getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, e.getMessage());
			} catch (Exception e) {
				logger.error("Unknown exception type", e);
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			}
		}
	}
}
