package org.nrg.xnat.restlet.extensions;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.JSONObjectRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.search.SubjectSummaryStatsI;
import org.nrg.xnat.services.search.SubjectSummaryStatsService;
import org.nrg.xnat.services.search.SubjectSummaryStatsService.CriteriaField;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to support real time querying of pre-defined subject fields as summary scores.
 * 
 *
 */
@XnatRestlet({"/services/cohort/subjects"})
public class SubjectStatRestlet extends SecureResource {

	private static final Log _log = LogFactory.getLog(SubjectStatRestlet.class);
    
    
    @Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {
		this.getResponse().setEntity(doRepresent());
	}

    @Override
    public Representation represent(Variant variant) throws ResourceException {
    	String columns="[";
    	
    	Map<String,CriteriaField> criteriaFields=SubjectSummaryStatsService.getCriteriaFields();
    	
     	for(String key: criteriaFields.keySet()){
     		if(!"".equals(columns)){
     			columns+=",";
     		}
     		columns+="'"+key +"':'"+criteriaFields.get(key).TYPE.toString()+"'\n";
     	}
     	columns+="]";
     	return new StringRepresentation(columns);
    }

    
        
    private static Map<String,String> fieldMap=new HashMap<String,String>();
    static {
    	fieldMap.put("cohorts", "cohorts");
    	fieldMap.put("Age at Baseline","age");
    	fieldMap.put("Sex","gender");
    	fieldMap.put("Education years","education");
    	fieldMap.put("Handedness","handedness");
    	fieldMap.put("Risk Gene","risk_gene");
    	fieldMap.put("Familial Gene","familial");
    	fieldMap.put("Other Risk Factor","other_risk");
    	fieldMap.put("MR Scan Types","mr_types");
    	fieldMap.put("PET Scan Types","pet_types");
    	fieldMap.put("MEG Scan Types","meg_types");
    	fieldMap.put("Diagnosis at Baseline","diagnosis");
    	fieldMap.put("Disease Conversion","conversion");
    	fieldMap.put("MMSE at Baseline","mmse");
    	fieldMap.put("MOCA at Baseline","moca");
    	fieldMap.put("CDR at Baseline","cdr");
    	fieldMap.put("Longitudinal Clinical Data","long_clin");
    	fieldMap.put("Longitudinal Imaging","long_image");
    }
    
    public SubjectStatRestlet(Context context, Request request, Response response) {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));


    }
    
    private Representation doRepresent(){
    	 if (_log.isDebugEnabled()) {
             _log.debug("Entering the session count represent() method");
         }

             try {
            	 final JSONObject json = buildStats();
     			return new JSONObjectRepresentation(MediaType.APPLICATION_JSON, json);
     		} catch (Exception e) {
     			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e);
     			return null;
     		}
    }
    private JSONObject buildStats() throws Exception {
    	String criteriaString=null;
    	//parse body
    	if(this.getRequest().isEntityAvailable()){
			Representation entity=this.getRequest().getEntity();
			if(RequestUtil.hasContent(entity)){
				criteriaString=IOUtils.toString(entity.getStream());
			}
    	}
    	
    	final SubjectSummaryStatsI summary=SubjectSummaryStatsService.create(criteriaString);
        
    	final JSONObject root = new JSONObject();
		root.put("Summary", summary.getSubjectCount());
		
		try {
			for(Map.Entry<String,String> entry : fieldMap.entrySet()){
				root.put(entry.getKey(), summary.getStat(entry.getValue()));
			}
			
			return root;
		} catch (Exception e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e);
			return null;
		}        
    }
}


