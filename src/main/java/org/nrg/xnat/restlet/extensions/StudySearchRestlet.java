package org.nrg.xnat.restlet.extensions;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.project.study.Study;
import org.nrg.xnat.services.project.study.StudyException;
import org.nrg.xnat.services.project.study.StudyService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.StringRepresentation;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author james@radiologics.com
 *
 */

/*
 * 
POST /data/study/filter
 * 
 */
@XnatRestlet({"/study/{ID}","/study/filter","/study/filter/{DETAILS}","/study/filterbyowner/{OWNERDETAILS}"})
public class StudySearchRestlet extends SecureResource {
    
    private static final ObjectMapper MAPPER = new ObjectMapper();

	static Logger logger = Logger.getLogger(StudySearchRestlet.class);
	final static String PROPERTIES="properties";
	final static String ID="id";
	final static String NAME="name";
	final static String TYPE="type";
	final static String INSERTDATE="insertDate";
	final static String ACCESSIBILITY="accessibility";
	final static String SECONDARY_ID="secondary_id";

	private UserI user; 
	
	final static Boolean SEARCHTERMNOTREQURED=Boolean.TRUE;

	final StudyService studyService;
	

	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public StudySearchRestlet(Context context, Request request, Response response) {
		super(context, request, response);
		this.studyService=XDAT.getContextService().getBean(StudyService.class);
		user = this.getUser();
	}
	
	@Override
	public boolean allowPost() {
		return true;
	}
	@Override
	public boolean allowDelete() {
		return false;
	}
	@Override
	public boolean allowGet() {
		return true;
	}
	@Override
	public boolean allowPut() {
		return false;
	}
	

	
	
	boolean validate(XDATUser user,String searchTerm) throws Exception{
		//
		if(StringUtils.isBlank(searchTerm)){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing search parameters");
			return false;
		}
		return true;
	}
	
	
	
	
	
	private void sendTableRepresentation(XFTTable table) {
        
		MediaType mt = overrideVariant(this.getPreferredVariant());
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		if (table != null) {
			params.put("totalRecords", table.size());
		}

		Map<String,Map<String,String>> cp = new Hashtable<String,Map<String,String>>();

		getResponse().setEntity(this.representTable(table, mt, params, cp));
		
	}
	void buildTable(List<Study> studies){
		ArrayList<String> columns=new ArrayList<String>();
        //columns.add(ID);
        columns.add(ID);
        columns.add(NAME);
        columns.add(TYPE);
        columns.add(INSERTDATE);
        columns.add(ACCESSIBILITY);
        columns.add(SECONDARY_ID);
        XFTTable table=new XFTTable();
        table.initTable(columns);
        
        for (Study study : studies) {
			 Object[] oarray = new Object[] { study.getId(),study.getName(),study.getType(),study.getInsertDate(),study.getAccessibility(),study.getSecondaryID()};
        		table.insertRow(oarray);
        }
        this.sendTableRepresentation(table);
	}

	@Override
	public void handleGet() {
		
		try {
			String studyID= (String) getRequest().getAttributes().get("ID");
			if(StringUtils.isBlank(studyID)){
				throw new Exception("Missing studyID parameter");
			}
			Study study=this.studyService.findById(user, studyID);
			if(study==null){
				throw new StudyException("Study not found!");
			}
			this.getResponse().setEntity(new StringRepresentation(MAPPER.writeValueAsString(study)));
			
			this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
			
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}
	@Override
	
	public void handlePost() {
		
		try {
			
			String detail= (String) getRequest().getAttributes().get("DETAILS");
			String ownerdetails= (String) getRequest().getAttributes().get("OWNERDETAILS");
			
				if(!StringUtils.isEmpty(detail)){
					List<Study> studies=this.studyService.findReadableByUser(user);
					if("true".equals(detail)){
						this.getResponse().setEntity(new StringRepresentation(MAPPER.writeValueAsString(studies)));
					}else{
						this.buildTable(studies);
					}
				}else if(!StringUtils.isEmpty(ownerdetails)){
					List<Study> studies=this.studyService.findEditableByUser(user);
					if("true".equals(ownerdetails)){
						this.getResponse().setEntity(new StringRepresentation(MAPPER.writeValueAsString(studies)));
					}else{
						this.buildTable(studies);
					}
				}
				this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
			
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}
	
	
}
