package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.investigator.Investigator;
import org.nrg.xnat.services.investigator.InvestigatorService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
//get /delete /put
@XnatRestlet({ "/projects/{PROJECT}/PI" ,"/projects/{PROJECT}/PI/{ID}"})
public class ProjectPrincipalInvestigatorRestlet extends SecureResource {
	private final InvestigatorService requests = XDAT.getContextService().getBean(InvestigatorService.class);
	static Logger logger = Logger.getLogger(ProjectPrincipalInvestigatorRestlet.class);
	private UserI user;

	public ProjectPrincipalInvestigatorRestlet(Context context, Request request,Response response) throws Exception {
		super(context, request, response);
		this.getVariants().add(new Variant(MediaType.ALL));
		user = this.getUser();
	}

	@Override
	public boolean allowDelete() {
		return true;
	}

	@Override
	public boolean allowPut() {
		return true;
	}

	@Override
	public boolean allowGet() {
		return true;
	}

	@Override
	public boolean allowPost() {
		return false;
	}

	@Override
	public void handleGet() {

		try {
			Investigator investigator = null;

			String project = (String) getRequest().getAttributes().get("PROJECT");

			if (!StringUtils.isBlank(project)) {
				investigator=requests.getPI(user, project);
			}
			if(investigator!=null){
				this.getResponse().setEntity(jsonRepresentation(investigator));
			}else{
				logger.error("Failed to get Principal Investigator.");
				this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Failed to find principal investigator");
			}
		} catch (Throwable exception) {
			logger.error("Failed to get publication.", exception);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Failed to get investigator");

		}
	}

	@Override
	public void handleDelete() {

		String project = (String) getRequest().getAttributes().get("PROJECT");

		if (project != null) {

			try {
				XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
				if (!(proj.canEdit(user))) {
					throw new Exception("User does not have permission to edit this project.");
				} else {
					 requests.deletePI(user, project);
				}
			} catch (Throwable e) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Exception removing investigator");
			}
		} else {
			logger.warn("Invalid ID or project.");
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Invalid ID or project.");

		}
	}

	@Override
	public void handlePut() {

		String project = (String) getRequest().getAttributes().get("PROJECT");
		String id = (String) getRequest().getAttributes().get("ID");

		if ((id != null) && (project != null)) {

			try {
				XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
				if (!(proj.canEdit(user))) {
					throw new Exception(
							"User does not have permission to edit this project.");
				} else {
					try {
						requests.addPI(user, id, project);
					} catch (Throwable e) {
						logger.error("Exception adding PI to project.", e);
						this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());

					}
				}
			} catch (Throwable e) {
				logger.error("Exception adding publication.", e);
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());

			}
		} else {
			logger.warn("Invalid PMID or project.");
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
					"Invalid PMID or project.");

		}
	}

	private Representation jsonRepresentation(
			final Investigator investigator) throws IOException {
		String json = requests.toJson(investigator);
		Representation r = new StringRepresentation(json);
		r.setMediaType(MediaType.APPLICATION_JSON);
		return r;
	}
}
