package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.investigator.Investigator;
import org.nrg.xnat.services.investigator.InvestigatorService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

@XnatRestlet({ "/projects/{PROJECT}/investigators/{ID}","/projects/{PROJECT}/investigators" })
public class ProjectInvestigatorsRestlet extends SecureResource {
	private final InvestigatorService requests = XDAT.getContextService()
			.getBean(InvestigatorService.class);
	static Logger logger = Logger.getLogger(ProjectInvestigatorsRestlet.class);
	UserI user;

	public ProjectInvestigatorsRestlet(Context context, Request request,Response response) throws Exception {
		super(context, request, response);
		this.getVariants().add(new Variant(MediaType.ALL));
		user = this.getUser();
	}

	@Override
	public boolean allowDelete() {
		return true;
	}

	@Override
	public boolean allowPut() {
		return true;
	}

	@Override
	public boolean allowGet() {
		return true;
	}

	@Override
	public boolean allowPost() {
		return false;
	}

	@Override
	public void handleGet() {

		try {
			List<Investigator> investigators = new ArrayList<>();

			String project = (String) getRequest().getAttributes().get("PROJECT");
			String id = (String) getRequest().getAttributes().get("ID");

			if (!StringUtils.isBlank(project)) {
				if (!StringUtils.isBlank(id)) {
					investigators.add(requests.getInvestigator(user, id, project));
				} else {
					investigators.addAll(requests.getInvestigatorsForProject(user, project));
				}
			}
			if(investigators.size()>0){
				this.getResponse().setEntity(jsonRepresentation(investigators));
			}
			else{
				logger.error("Failed to get investigator.");
				this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Failed to find investigator");
			}
		} catch (Throwable exception) {
			logger.error("Failed to get investigator.", exception);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Failed to get investigator");

		}
	}

	@Override
	public void handleDelete() {

		String project = (String) getRequest().getAttributes().get("PROJECT");

		String id = (String) getRequest().getAttributes().get("ID");

		if ((id != null) && (project != null)) {

			try {
				XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(
						project, user, false);
				if (!(proj.canEdit(user))) {
					throw new Exception(
							"User does not have permission to edit this project.");
				} else {
					 requests.delete(user,id, project);
				}
			} catch (Throwable e) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Exception removing investigator");
			}
		} else {
			logger.warn("Invalid PMID or project.");
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
					"Invalid PMID or project.");

		}
	}

	@Override
	public void handlePut() {

		String project = (String) getRequest().getAttributes().get("PROJECT");
		String id = (String) getRequest().getAttributes().get("ID");

		if ((id != null) && (project != null)) {

			try {
				XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
				if (!(proj.canEdit(user))) {
					throw new Exception("User does not have permission to edit this project.");
				} else {
					try {
						requests.add(user, id, project);
					} catch (Throwable e) {
						logger.error(
								"Exception adding investigator to project.", e);
						this.getResponse().setStatus(
								Status.SERVER_ERROR_INTERNAL, e.getMessage());

					}
				}
			} catch (Throwable e) {
				logger.error("Exception adding publication.", e);
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
						e.getMessage());

			}
		} else {
			logger.warn("Invalid PMID or project.");
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
					"Invalid PMID or project.");

		}
	}

	private Representation jsonRepresentation(
			final List<Investigator> investigators) throws IOException {
		String json = requests.toJson(investigators);
		Representation r = new StringRepresentation(json);
		r.setMediaType(MediaType.APPLICATION_JSON);
		return r;
	}
	
	
}
