package org.nrg.xnat.restlet.extensions;

import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.publication.PublicationService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Variant;
import org.apache.log4j.Logger;

/**
 * AddPublicationRestlet
 *
 * @author rherri01
 * @since 3/5/2014
 */
@XnatRestlet({"/services/addPublication"})
public class AddPublicationRestlet extends SecureResource {
    static Logger logger = Logger.getLogger(AddPublicationRestlet.class);
    private UserI user;

    public AddPublicationRestlet(Context context, Request request, Response response) throws Exception{
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
        user = this.getUser();
    }

    @Override public boolean allowDelete() { return false; }
    @Override public boolean allowPut()    { return true; }
    @Override public boolean allowGet()    { return false; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handlePut(){
        String articleTitle = this.getQueryVariable("articleTitle");
        String authors      = this.getQueryVariable("authors");
        String publication  = this.getQueryVariable("publication");
        String date         = this.getQueryVariable("date");
        String pmid         = this.getQueryVariable("pmid");
        String project      = this.getQueryVariable("project");
        String doi          = this.getQueryVariable("doi");

        if((pmid != null) && (project != null)){
            try{
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(!proj.canEdit(user)){
                    throw new Exception("User does not have permission to edit this project.");
                }
                else{
                    Publication newPublication = new Publication(articleTitle, authors, publication, date, pmid, project, doi);
                    try{
                        XDAT.getContextService().getBean(PublicationService.class).create(newPublication);
                    }catch(Throwable e){
                        logger.error("Exception creating publication in database.",e);
                    }
                }
            }
            catch(Throwable e) {
                logger.error("Exception adding publication.",e);
            }
        }
        else{
            logger.warn("Invalid PMID or project.");
        }
    }
}
