package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.nrg.xapi.entities.CohortAccessRequest;
import org.nrg.xapi.entities.CohortStatus;
import org.nrg.xapi.entities.Subcohort;
import org.nrg.xapi.services.CARManagementService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatInvestigatordataI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.search.SubjectSummaryStatsI;
import org.nrg.xnat.services.search.SubjectSummaryStatsService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author Tim Olsen <tim@radiologics.com>
 * 
 * Created for the DPUK project to save search criteria for particular cohort access requests
 * 
 *
 */
@XnatRestlet({"/services/cars/{ID}/search"})
public class SavedCARsSearchRestlet extends SecureResource {

	private static final Log _log = LogFactory.getLog(SavedCARsSearchRestlet.class);
	
	private Integer carId=null;
	
	private UserI user;

	public SavedCARsSearchRestlet(Context context, Request request, Response response) {
        super(context, request, response);
        String carS=(String)getParameter(request,"ID");
		if(NumberUtils.isNumber(carS)){
			carId= Integer.parseInt(carS);
		}else{
			response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return;
		}
        this.getVariants().add(new Variant(MediaType.ALL));
        
        user = this.getUser();
	}
    
    @Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {
		String criteriaString=null;
		//parse body which should include search criteria as it was used on the search page
    	if(this.getRequest().isEntityAvailable()){
			Representation entity=this.getRequest().getEntity();
			if(RequestUtil.hasContent(entity)){
				try {
					criteriaString=IOUtils.toString(entity.getStream());
				} catch (IOException e) {
		 			_log.error("",e);
		 			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e);
		 			return;
				}
			}
    	}

        try {
        	//CAR entry should already exist
        	CARManagementService _service =XDAT.getContextService().getBean(CARManagementService.class);
	    	final CohortAccessRequest car = _service.retrieve(carId);
	        if (car == null) {
	            this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
	            return ;
	        }
	        
	        //only site admins or the original CAR user can modify this.
	        if (!(StringUtils.equals(user.getUsername(),car.getUsername()) || Roles.isSiteAdmin(user))) {
	            this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
	            return ;
	        }
	        
			
	    	car.setSearch(criteriaString);
	    	
	    	//use the search api service to identify the subjects to be shared.
 			SubjectSummaryStatsI stats=SubjectSummaryStatsService.create((StringUtils.equals("{}",car.getSearch()))?null:car.getSearch());
 			List<Object[]> subjects=stats.getSubjectIDs();
 			
 			Map<String,StudyInfo> counts=Maps.newHashMap();
 			for(Object[] subject: subjects){
 				String project=(String) subject[1];
 				if(!counts.containsKey(project)){
 					counts.put(project, new StudyInfo((String) subject[0], (String) subject[1], (String) subject[3], (String) subject[4]));
 				}
 				
 				counts.get(project).increment();
 			}
 			
 			List<Subcohort> cohorts=car.getCohorts();
 			
 			//update existing cohorts
 			for(Subcohort cohort:cohorts){
 				StudyInfo studyInfo=counts.get(cohort.getProject());
 				if(studyInfo!=null){
	 				cohort.setSubjectCount(studyInfo.count);
	 				
	 				_service.createOrUpdate(cohort);
	 				counts.remove(cohort.getProject());
 				}else{
 					_service.delete(cohort);
 				}
 			}
 			
 			//reset in case some were deleted
 			cohorts=car.getCohorts();
 			 			
 			//add new cohorts
 			for(String key: counts.keySet()){
 				StudyInfo mapping=counts.get(key);
 				
 				boolean matched=false;
 				for(Subcohort existing:cohorts){
 					if(StringUtils.equals(existing.getProject(),mapping.project)){
 						matched=true;
 					}
 				}
 				
 				if(matched){
 					continue;
 				}
 				
 				XnatProjectdata proj=XnatProjectdata.getProjectByIDorAlias(mapping.study, null, false);
 				
 				Subcohort cohort=new Subcohort();
 				cohort.setProject(mapping.project);
 				cohort.setStudy(mapping.study);
 				cohort.setStudyDisplay(mapping.studyDisplay);
 				cohort.setProjectDisplay(mapping.projectDisplay);
 				List<String> investigators=Lists.newArrayList();
 				if(proj.getPi()!=null && StringUtils.isNotBlank(proj.getPi().getEmail())){
 					investigators.add(proj.getPi().getEmail());
 				}
 				for(XnatInvestigatordataI investigator: proj.getInvestigators_investigator()){
 					if(StringUtils.isNotBlank(investigator.getEmail())){
 						investigators.add(investigator.getEmail());
 					}
 				}
 				if(investigators.size()>0){
 					cohort.setOwners(Arrays.asList(new String[]{proj.getPi().getEmail()}));
 				}
 				cohort.setStatus(CohortStatus.Pending);
 				cohort.setSubjectCount(mapping.count);
 				cohort.setRequest(car);
 				
 				_service.createOrUpdate(cohort);
 				cohorts.add(cohort);
 			}
 			
 			car.setResults(subjects);
 			_service.update(car);
 			
 			this.getResponse().setEntity(new StringRepresentation("Matched "+ subjects.size()+ " subjects."));
 		} catch (JSONException e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e);
 		} catch (Exception e) {
 			_log.error("",e);
 			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e);
 		}
	}
	
	public static class StudyInfo{
		public String study;
		public String studyDisplay;
		public String project;
		public String projectDisplay;
		public Integer count=0;
		
		public StudyInfo(String s, String p, String sd, String pd){
			study=s;
			project=p;
			studyDisplay=sd;
			projectDisplay=pd;
		}
		
		public void increment(){
			count++;
		}
	}

    @Override
    public Representation represent(Variant variant) throws ResourceException {
    	logger.error("CAR ID:"+carId);
    	try {
			final CohortAccessRequest car = XDAT.getContextService().getBean(CARManagementService.class).retrieve(carId);
			if (car == null) {
			    this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			    return null;
			}
			
			
			if (!(StringUtils.equals(user.getUsername(),car.getUsername()) || Roles.isSiteAdmin(user))) {
			    this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN);
			    return null;
			}
			
			final String search= car.getSearch();
			return new StringRepresentation(search);
		} catch (Exception e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return null;
		}
    }
}


