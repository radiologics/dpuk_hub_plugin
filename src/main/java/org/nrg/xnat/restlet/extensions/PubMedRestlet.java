package org.nrg.xnat.restlet.extensions;

import org.apache.log4j.Logger;
import org.nrg.xnat.restlet.XnatRestlet;
import org.restlet.resource.Representation;
import org.nrg.xnat.restlet.representations.PubMedXMLRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Variant;
import java.net.URL;
import java.net.URLDecoder;

@XnatRestlet({"/services/pubMed/{PMID}"})
public class PubMedRestlet extends SecureResource {
    private static final String URL_ENCODING = "UTF-8";
    static Logger logger = Logger.getLogger(PubMedRestlet.class);

    public PubMedRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }

    @Override
    public Representation represent(Variant variant) {
        URL url = null;
        try {
            String pmid= "";
            pmid= URLDecoder.decode((String) getParameter(getRequest(), "PMID"), URL_ENCODING);

            url = new URL("https://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id="+pmid+"&retmode=xml");
        } catch (Exception e) {
            logger.error("Error obtaining publication from PubMed.", e);
        }

       return new PubMedXMLRepresentation(url);
    }


}
