package org.nrg.xnat.restlet.extensions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.publication.PublicationService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

@XnatRestlet({"/projects/{PROJECT}/publications/{PMID}","/projects/{PROJECT}/publications"})
public class ProjectPublicationsRestlet extends SecureResource {
    private final PublicationService publicationService = XDAT.getContextService().getBean(PublicationService.class);
    static Logger logger = Logger.getLogger(ProjectPublicationsRestlet.class);
    private UserI user;

    public ProjectPublicationsRestlet(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
        user = this.getUser();
    }

    @Override public boolean allowDelete() { return true; }
    @Override public boolean allowPut()    { return true; }
    @Override public boolean allowGet()    { return true; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handleGet(){

        try {
            List<Publication> publications = new ArrayList<Publication>();

            String project=(String)getRequest().getAttributes().get("PROJECT");
            String pmid=(String)getRequest().getAttributes().get("PMID");


            if (!StringUtils.isBlank(project)) {
            	if(!StringUtils.isBlank(pmid)){
            		publications.addAll(publicationService.getPublicationsByIdAndProject(pmid, project));
            	}else{
            		publications.addAll(publicationService.getPublicationsForProject(project));
            	}
            }
            this.getResponse().setEntity(jsonRepresentation(publications));
        } catch (Throwable exception) {
            logger.error("Failed to get publication.", exception);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Failed to get publication");

        }
    }

    @Override public void handleDelete(){

        String project=(String)getRequest().getAttributes().get("PROJECT");
        String pmid=(String)getRequest().getAttributes().get("PMID");



        if((pmid != null) && (project != null)){

            try{
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(proj == null){
                    throw new Exception("Unknown project "+ project);
                }
                else if(!(proj.canEdit(user))){
                    throw new Exception("User does not have permission to edit this project.");
                }
                else{
                    List<Publication> oldPublications = publicationService.getPublicationsByIdAndProject(pmid, project);
                    for(Publication oldPublication : oldPublications){
                        XDAT.getContextService().getBean(PublicationService.class).delete(oldPublication);
                    }
                    this.getResponse().setStatus(Status.SUCCESS_OK);
                }
            }
            catch(Throwable e) {
                logger.error("Exception removing publication.",e);
    			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Exception removing publication");
            }
        }
        else{
            logger.warn("Invalid PMID or project."+ pmid+" "+ project);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Invalid PMID or project.");

        }
    }

    @Override public void handlePut(){
        String articleTitle = SecureResource.getQueryVariable("articleTitle", getRequest());
        String authors = SecureResource.getQueryVariable("authors", getRequest());
        String publication = SecureResource.getQueryVariable("publication", getRequest());
        String date = SecureResource.getQueryVariable("date", getRequest());
        String doi = SecureResource.getQueryVariable("doi", getRequest());

        //lets try another location..
        if(StringUtils.isBlank(articleTitle))
        	articleTitle = this.getBodyVariable("articleTitle");
        if(StringUtils.isBlank(authors))
        	authors = this.getBodyVariable("authors");
        if(StringUtils.isBlank(publication))
        	publication = this.getBodyVariable("publication");
        if(StringUtils.isBlank(date))
        	date = this.getBodyVariable("date");
        if(StringUtils.isBlank(doi))
        	doi = this.getBodyVariable("doi");


        String project=(String)getRequest().getAttributes().get("PROJECT");
        String pmid=(String)getRequest().getAttributes().get("PMID");



        if((pmid != null) && (project != null)){

            try{
                XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(project, user, false);
                if(proj == null){
                    throw new Exception("Unknown project.");
                }else if(!(proj.canEdit(user))){
                	throw new Exception("User does not have permission to edit this project.");
                }else{
                    Publication newPublication = new Publication(articleTitle, authors, publication, date, pmid, proj.getId(), doi);
                    try{
                        publicationService.create(newPublication);
                        this.getResponse().setStatus(Status.SUCCESS_OK);
                    }catch(Throwable e){
                        logger.error("Exception creating publication in database.",e);
            			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
                    }
                }
            }
            catch(Throwable e) {
                logger.error("Exception adding publication.",e);
    			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
            }
        }
        else{
            logger.warn("Invalid PMID or project."+ pmid+" "+ project);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Invalid PMID or project.");

        }
    }


    private Representation jsonRepresentation(final List<Publication> publications) throws IOException {
        String json = publicationService.toJson(publications);
        Representation r = new StringRepresentation(json);
        r.setMediaType(MediaType.APPLICATION_JSON);
        return r;
    }
}
