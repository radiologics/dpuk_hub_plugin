/*
 *org.nrg.xnat.restlet.extensions.UserTokenRestlet
 * XNAT http://www.xnat.org
 * Copyright (c) 2015, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 09/03/15 3:19 PM
 */
package org.nrg.xnat.restlet.extensions;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import javax.servlet.http.HttpServletRequest;


@XnatRestlet({"/token"})
public class UserTokenRestlet extends SecureResource {
	protected XDATUser user = null;
	HttpServletRequest http_request = null;
	final String XNAT_CSRF="XNAT_CSRF";
	private final String USER_ATTRIBUTE = "user";
	
	public UserTokenRestlet(Context context, Request request, Response response) {
		super(context, request, response);

		getVariants().add(new Variant(MediaType.TEXT_PLAIN));

		// copy the user from the request into the session
		getHttpSession().setAttribute(USER_ATTRIBUTE,getRequest().getAttributes().get(USER_ATTRIBUTE));
		

	}

	@Override
	public boolean allowDelete() {
		return false;
	}

	@Override
	public boolean allowPost() {
		return false;
	}

	@Override
	public void removeRepresentations() throws ResourceException {
		getHttpSession().invalidate();
	}

	@Override
	public void acceptRepresentation(Representation entity)
			throws ResourceException {
		getResponse().setEntity(sessionIdRepresentation());
	}

	@Override
	public Representation represent(Variant variant) throws ResourceException {
		return sessionIdRepresentation();
	}

	private Representation sessionIdRepresentation() {
		if (XDAT.getBoolSiteConfigurationProperty("allowTokenRetrieval", false)){
			return new StringRepresentation((String)getHttpSession().getAttribute(XNAT_CSRF),MediaType.TEXT_PLAIN);
		}else{
			return new StringRepresentation("",MediaType.TEXT_PLAIN);
		}
	}
	}