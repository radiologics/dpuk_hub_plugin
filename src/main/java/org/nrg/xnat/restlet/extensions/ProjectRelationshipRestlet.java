package org.nrg.xnat.restlet.extensions;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.project.relationship.ProjectRelationship;
import org.nrg.xnat.services.project.relationship.ProjectRelationshipService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

/**
 * @author james@radiologics.com
 *
 * Implementation of the User Roles functionality.  The post method adds or removes role for the specified user.
 */

/*
 * 
GET /data/projects/PROJ/relations (returns standard XNAT XFTTable formatted list)
PUT /data/projects/PROJ/relations/PROJ2; BODY=JSON properties (create a relationship or modify an existing one)
GET /data/projects/PROJ/relations/PROJ2/properties (returns JSON properties)
DELETE /data/projects/PROJ/relations/PROJ2/type/test (to remove a relationship)
 * 
 */
@XnatRestlet({
	"/projects/{PROJ1}/relations/{PROJ2}/type/{TYPE}",
	"/projects/{PROJ1}/relations/{PROJ2}/type/{TYPE}/properties",
	"/projects/{PROJ1}/relations/{PROJ2}",
	"/projects/{PROJ1}/relations"})
public class ProjectRelationshipRestlet extends SecureResource {
    
	static Logger logger = Logger.getLogger(ProjectRelationshipRestlet.class);
	final static String PROPERTIES="properties";
	final static String ID="ID";
	final static String PROJECT1="PROJECT1";
	final static String PROJECT2="PROJECT2";
	final static String TYPE="TYPE";
	private UserI user;
	
	ProjectRelationshipService projectRelationshipService;
	

	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public ProjectRelationshipRestlet(Context context, Request request, Response response) {
		super(context, request, response);
		user = this.getUser();
	}

	private ProjectRelationshipService getProjectRelationshipService() {
        if (projectRelationshipService == null) {
        	projectRelationshipService = XDAT.getContextService().getBean(ProjectRelationshipService.class);
        }
        return projectRelationshipService;
    }
	
	
	@Override
	public boolean allowPost() {
		return true;
	}
	@Override
	public boolean allowDelete() {
		return true;
	}
	@Override
	public boolean allowGet() {
		return true;
	}
	@Override
	public boolean allowPut() {
		return true;
	}
	

	@Override
	public void handleDelete() {
		
		try {
			String proj1 = (String) getRequest().getAttributes().get("PROJ1");
			String proj2 = (String) getRequest().getAttributes().get("PROJ2");
			String type = (String) getRequest().getAttributes().get("TYPE");

			if(validate(user, proj1, proj2, type)==true){
					this.getProjectRelationshipService().delete(proj1,proj2,type);
					this.getResponse().setEntity(new StringRepresentation(""));
					this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
			}
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}
	
	
	boolean validate(UserI user, String proj1,String proj2,String type) throws Exception{
		
		// Sanity Check
		if(proj1==null){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing project parameter");
			return false;
		}
		
		// Does the project exist in XNAT. If it does exist, check to see if this user allowed to edit the project.
		XnatProjectdata xproj1 = XnatProjectdata.getProjectByIDorAlias(proj1, user, false);
		if(xproj1==null){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Project Not Found, "+ proj1);
		}
		
		if(!xproj1.canEdit(user)){
			this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Access Denied to project "+ proj1);
			return false;
		}
		

		// Sanity Check
		if(proj2==null){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing project parameter");
			return false;
		}
		
		// Does the project exist in XNAT. If it does exist, check to see if this user allowed to edit the project.
		XnatProjectdata xproj2 = XnatProjectdata.getProjectByIDorAlias(proj2, user, false);
		if(xproj2==null){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Project Not Found, "+ proj2);
		}
		
		if(!xproj2.canEdit(user)){
			this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Access Denied to "+ proj2);
			return false;
		}

		// Sanity Check
		if(type==null){
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Type Not Found, "+ type);
			return false;
		}
		return true;
	}
	
	
	@Override
	//additional properties from post.
	//get table representation. will get properties from extra pr
	public void handleGet() {
		
		try {
			String proj1 = (String) this.getRequest().getAttributes().get("PROJ1");
			String proj2 = (String) this.getRequest().getAttributes().get("PROJ2");
			String type = (String) this.getRequest().getAttributes().get("TYPE");
			String properties = (String)this.getHttpServletRequest().getPathInfo();
			
			boolean props =false;
			
			if(validate(user, proj1, proj2, type)==true && properties.endsWith(PROPERTIES)){
				props=true;
			}

			ProjectRelationship projectRelationship=new ProjectRelationship();
			projectRelationship.setProject1(proj1);
			projectRelationship.setProject2(proj2);
			projectRelationship.setType(type);
			
			List<ProjectRelationship> relationships=this.getProjectRelationshipService().findAllBy(proj1,proj2,type);
			
			for (ProjectRelationship projectRelationship2 : relationships) {
				logger.warn(projectRelationship2.toString());
			}  
			
			//convert to table representation stuff..
			if(props & relationships.size()==1){
				this.getResponse().setEntity(new StringRepresentation(relationships.get(0).getProperties()));
			}else{
				this.buildTable(relationships);
			}
			
			this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}
	
	
	private void sendTableRepresentation(XFTTable table) {
        
		MediaType mt = overrideVariant(this.getPreferredVariant());
		Hashtable<String, Object> params = new Hashtable<String, Object>();
		if (table != null) {
			params.put("totalRecords", table.size());
		}

		Map<String,Map<String,String>> cp = new Hashtable<String,Map<String,String>>();

		getResponse().setEntity(this.representTable(table, mt, params, cp));
		
	}
	void buildTable(List<ProjectRelationship> relations){
		ArrayList<String> columns=new ArrayList<String>();
        columns.add(ID);
        columns.add(PROJECT1);
        columns.add(PROJECT2);
        columns.add(TYPE);
        columns.add(PROPERTIES);
        

        XFTTable table=new XFTTable();
        table.initTable(columns);
        
        for (ProjectRelationship projectRelationship : relations) {
        	if(XnatProjectdata.getProjectByIDorAlias(projectRelationship.getProject2(), user, false)!=null && XnatProjectdata.getProjectByIDorAlias(projectRelationship.getProject1(), user, false)!=null){
   			 	Object[] oarray = new Object[] { projectRelationship.getId(),projectRelationship.getProject1(),projectRelationship.getProject2(),projectRelationship.getType(),projectRelationship.getProperties()};
           		table.insertRow(oarray);
        	}
        }
        this.sendTableRepresentation(table);
	}


	@Override
	//additional properties from post.
	public void handlePut() {
		
		try {
			String proj1 = (String) getRequest().getAttributes().get("PROJ1");
			String proj2 = (String) getRequest().getAttributes().get("PROJ2");
			String type = (String) getRequest().getAttributes().get("TYPE");
			String jsonProperties = getRequest().getEntity().getText();
			
			
			if(validate(user, proj1, proj2, type)==true){

				ProjectRelationship projectRelationship=new ProjectRelationship();
				projectRelationship.setProject1(proj1);
				projectRelationship.setProject2(proj2);
				projectRelationship.setType(type);
	            if (StringUtils.isNotBlank(jsonProperties)) {
	            	projectRelationship.setProperties(jsonProperties);
	            }
				
				this.getProjectRelationshipService().addOrUpdateRelationship(projectRelationship);
			
				this.getResponse().setEntity(new StringRepresentation(""));
				this.getResponse().setStatus(Status.SUCCESS_ACCEPTED);
			}
		} catch (Throwable e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
			return;
		}
	}
	@Override
	public Representation represent(Variant variant) throws ResourceException {	
		MediaType mt = overrideVariant(variant);
		Hashtable<String,Object> params=new Hashtable<String,Object>();
		
		XFTTable table=new XFTTable();
		table.initTable(new String[]{"role"});
		
		
		
		if(table!=null)params.put("totalRecords", table.size());
		return this.representTable(table, mt, params);
	}
}
