package org.nrg.xnat.restlet.extensions;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

@XnatRestlet({"/services/current_user"})
public class StudyReleases extends SecureResource {
	UserI user;
	public StudyReleases(Context context, Request request, Response response) {
		super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
        user = this.getUser();
	}
	
    @Override
    public Representation represent(Variant variant) throws ResourceException {
    	return new StringRepresentation(user.getLogin());
    }
}
