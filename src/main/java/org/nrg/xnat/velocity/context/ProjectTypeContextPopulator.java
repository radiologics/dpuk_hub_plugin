package org.nrg.xnat.velocity.context;
import org.nrg.xnat.services.project.type.ProjectType;
import org.nrg.xnat.velocity.context.PostAddProjectContextPopulator;
import org.springframework.stereotype.Component;

@Component("projectTypeContextPopulator")
public class ProjectTypeContextPopulator implements PostAddProjectContextPopulator{
		public Object getObject() {
			return ProjectType.getProjectTypes();
		}
		public String getName() {
			return "allProjectTypes";
		}
}